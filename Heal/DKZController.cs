﻿using System;
using System.IO.Ports;

namespace Heal
{
    class DKZController
    {
        private static DKZController mDKZ;
        public SerialPort Serial { get; set; }
        public static DKZController Instance
        {
            get
            {
                if(mDKZ == null)
                {
                    mDKZ = new DKZController();
                    mDKZ.Serial = new SerialPort();
                    mDKZ.Serial.BaudRate = 19200;
                }
                return mDKZ;
            }
        }


        public int Open()
        {
            if(!Serial.IsOpen)
            {
                try
                {
                    Serial.WriteTimeout = 2000;
                    Serial.ReadTimeout = 2000;
                    Serial.Open();
                    
                }
                catch
                {
                    return -1;
                }
                return 0;
            }
            return -2;
        }
        public int Close()
        {
            if (Serial.IsOpen)
            {
                try
                {
                    Serial.Close();
                }
                catch
                {
                    return -1;
                }
                return 0;
            }
            return -2;
        }


        public int ActiveOne(int Channel, int Mode)
        {
            byte[] data = new byte[] { 0xab, 0xba, 0x03, 0x32, 0x00, 0x00};
            data[4] = Convert.ToByte(Channel - 1);
            data[5] = Convert.ToByte(Mode);
            bool succ = false;
            try
            {
                Serial.Write(data, 0, data.Length);
                int res = Serial.ReadChar();
                if (res == 'o')
                    succ = true;
            }
            catch
            {
                return -1;
            }
            if (succ)
                return 0;
            else
                return -2;
        }
        public int SetOne(int Channel, int Value)
        {
            byte[] data = new byte[] { 0xab, 0xba, 0x03, 0x31, 0x00, 0x00 };
            data[4] = Convert.ToByte(Channel - 1);
            data[5] = Convert.ToByte(Value);
            bool succ = false;
            try
            {
                Serial.Write(data, 0, data.Length);
                int res = Serial.ReadChar();
                if (res == 'o')
                    succ = true;
            }
            catch
            {
                return -1;
            }
            if (succ)
                return 0;
            else
                return -2;
        }
        public int SetFour(int Value1, int Value2, int Value3, int Value4)
        {
            byte[] data = new byte[] { 0xab, 0xba, 0x05, 0x33, 0x00, 0x00, 0x00, 0x00 };
            data[4] = Convert.ToByte(Value1);
            data[5] = Convert.ToByte(Value2);
            data[6] = Convert.ToByte(Value3);
            data[7] = Convert.ToByte(Value4);
            bool succ = false;
            try
            {
                Serial.Write(data, 0, data.Length);
                int res = Serial.ReadChar();
                if (res == 'o')
                    succ = true;
            }
            catch
            {
                return -1;
            }
            if (succ)
                return 0;
            else
                return -2;
        }
        public int ActiveFour(int Mode1, int Mode2, int Mode3, int Mode4)
        {
            byte[] data = new byte[] { 0xab, 0xba, 0x05, 0x34, 0x00, 0x00, 0x00, 0x00 };
            data[4] = Convert.ToByte(Mode1);
            data[5] = Convert.ToByte(Mode2);
            data[6] = Convert.ToByte(Mode3);
            data[7] = Convert.ToByte(Mode4);
            bool succ = false;
            try
            {
                Serial.Write(data, 0, data.Length);
                int res = Serial.ReadChar();
                if (res == 'o')
                    succ = true;
            }
            catch
            {
                return -1;
            }
            if (succ)
                return 0;
            else
                return -2;
        }

    }
}
