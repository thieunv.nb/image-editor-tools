﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Heal
{
    public partial class MainForm : Form
    {
        private HikVisionCameraSDK mCamera = HikVisionCameraSDK.Instance;
        private modeProcessing mMode = modeProcessing.ImageSource;
        private MyLog mLog = MyLog.Instance;
        private System.Timers.Timer mTimer = new System.Timers.Timer(10);
        private bool mTimerIsRun = false;
        public MainForm()
        {
            InitializeComponent();
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            rbFromCamera_CheckedChanged(null, null);
            label1_Click(null, null);
            cRotation_CheckedChanged(null, null);
            ColorSpaceChanged();

        }
        private void LoadUI()
        {
            
        }
        private void OntimedEvent(object s, System.Timers.ElapsedEventArgs e)
        {
            mTimer.Enabled = false;
            using (Bitmap bm = mCamera.GetOneBitmap_NET(1000))
            {
                if (bm != null)
                {
                    using (Image<Bgr, byte> img = new Image<Bgr, byte>(bm))
                    {
                        UtilsConfigurations.SetImage(img);
                        UtilsConfigurations.Processing(mMode);
                        ShowImg();
                    }
                }
            }

            mTimer.Enabled = mTimerIsRun;
        }
        private void btBrowser_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "Images File | *.png; *.jpg; *.bmp; *.jpeg; *.tiff; *.gif";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    txtFilePath.Text = ofd.FileName;
                    using (Image<Bgr, byte> img = new Image<Bgr, byte>(ofd.FileName))
                    {
                        UtilsConfigurations.SetImage(img);
                        UtilsConfigurations.Processing(mMode);
                        int width = img.Width;
                        int height = img.Height;
                        lbShape.Text = string.Format("{0}x{1} Pixel", width, height);
                        ShowImg();
                    }

                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            string oldSelection = string.Empty;
            string[] device = mCamera.GetAllCameraName_NET();
            if (cbListCamera.SelectedIndex != -1)
            {
                cbListCamera.SelectedItem.ToString();
            }
            cbListCamera.Items.Clear();
            if (device != null)
            {
                cbListCamera.Items.AddRange(device);
                if (device.Contains(oldSelection))
                {
                    cbListCamera.SelectedItem = oldSelection;
                }
                else
                {
                    cbListCamera.SelectedIndex = 0;
                }
            }
        }

        private void rbFromCamera_CheckedChanged(object sender, EventArgs e)
        {
            bool mode = rbFromCamera.Checked;
            cbListCamera.Enabled = mode;
            btConnect.Enabled = mode;
            txtFilePath.Enabled = !mode;
            btBrowser.Enabled = !mode;
            btKeepChanged.Enabled = !mode;
        }
        
        private void ShowImg()
        {
            this.Invoke(new Action(() => {
                if(imgBoxConfiguration.Image != null)
                {
                    imgBoxConfiguration.Image.Dispose();
                }
                try
                {
                    Image<Bgr, byte> temp = null;
                    switch (mMode)
                    {
                        case modeProcessing.ImageSource:
                            if (UtilsConfigurations.ImageSource == null)
                                break;
                            temp = UtilsConfigurations.ImageSource.Copy();
                            break;
                        case modeProcessing.ImgEqualization:
                            if (UtilsConfigurations.ImgEqualization == null)
                                break;
                            temp = UtilsConfigurations.ImgEqualization.Copy();
                            break;
                        case modeProcessing.ImageGray:
                            if (UtilsConfigurations.ImageGray == null)
                                break;
                            temp = UtilsConfigurations.ImageGray.Convert<Bgr, byte>().Copy();
                            break;
                        case modeProcessing.ImageBlur:
                            if (UtilsConfigurations.ImageBlur == null)
                                break;
                            temp = UtilsConfigurations.ImageBlur.Copy();
                            break;
                        case modeProcessing.ImageBlurGaussian:
                            if (UtilsConfigurations.ImageBlurGaussian == null)
                                break;
                            temp = UtilsConfigurations.ImageBlurGaussian.Copy();
                            break;
                        case modeProcessing.ImageBlurMedian:
                            if (UtilsConfigurations.ImageBlurMedian == null)
                                break;
                            temp = UtilsConfigurations.ImageBlurMedian.Copy();
                            break;
                        case modeProcessing.ImgBilateral:
                            if (UtilsConfigurations.ImgBilateral == null)
                                break;
                            temp = UtilsConfigurations.ImgBilateral.Copy();
                            break;
                        case modeProcessing.ImageThreshold:
                            if (UtilsConfigurations.ImageThreshold == null)
                                break;
                            temp = UtilsConfigurations.ImageThreshold.Convert<Bgr, byte>().Copy();
                            break;
                        
                        case modeProcessing.ImageThresholdInv:
                            if (UtilsConfigurations.ImageThresholdInv == null)
                                break;
                            temp = UtilsConfigurations.ImageThresholdInv.Convert<Bgr, byte>().Copy();
                            break;
                        case modeProcessing.ImageThresholdGaussian:
                            if (UtilsConfigurations.ImageThresholdGaussian == null)
                                break;
                            temp = UtilsConfigurations.ImageThresholdGaussian.Convert<Bgr, byte>().Copy();
                            break;
                        case modeProcessing.ImageThresholdMean:
                            if (UtilsConfigurations.ImageThresholdMean == null)
                                break;
                            temp = UtilsConfigurations.ImageThresholdMean.Convert<Bgr, byte>().Copy();
                            break;
                        case modeProcessing.ImageThresholdOstu:
                            if (UtilsConfigurations.ImageThresholdOstu == null)
                                break;
                            temp = UtilsConfigurations.ImageThresholdOstu.Convert<Bgr, byte>().Copy();
                            break;
                        case modeProcessing.ImageLaplacian:
                            if (UtilsConfigurations.ImageLaplacian == null)
                                break;
                            temp = UtilsConfigurations.ImageLaplacian.Convert<Bgr, byte>().Copy();
                            break;
                        case modeProcessing.ImageSobelX:
                            if (UtilsConfigurations.ImageSobelX == null)
                                break;
                            temp = UtilsConfigurations.ImageSobelX.Convert<Bgr, byte>().Copy();
                            break;
                        case modeProcessing.ImageSobelY:
                            if (UtilsConfigurations.ImageSobelY == null)
                                break;
                            temp = UtilsConfigurations.ImageSobelY.Convert<Bgr, byte>().Copy();
                            break;
                        case modeProcessing.ImageCanny:
                            if (UtilsConfigurations.ImageCanny == null)
                                break;
                            temp = UtilsConfigurations.ImageCanny.Convert<Bgr, byte>().Copy();
                            break;
                        case modeProcessing.ImageDilate:
                            if (UtilsConfigurations.ImageDilate == null)
                                break;
                            temp = UtilsConfigurations.ImageDilate.Convert<Bgr, byte>().Copy();
                            break;
                        case modeProcessing.ImageErode:
                            if (UtilsConfigurations.ImageErode == null)
                                break;
                            temp = UtilsConfigurations.ImageErode.Convert<Bgr, byte>().Copy();
                            break;
                        case modeProcessing.ImageOpen:
                            if (UtilsConfigurations.ImageOpen == null)
                                break;
                            temp = UtilsConfigurations.ImageOpen.Convert<Bgr, byte>().Copy();
                            break;
                        case modeProcessing.ImageClose:
                            if (UtilsConfigurations.ImageClose == null)
                                break;
                            temp = UtilsConfigurations.ImageClose.Convert<Bgr, byte>().Copy();
                            break;
                        default:
                            break;
                    }
                    if(temp != null && !imgBoxConfiguration.IsDisposed)
                    {
                        if (cRotation.Checked)
                        {
                            Point center = new Point((int)nRotationCenterX.Value, (int)nRotationCenterY.Value);
                            double angle = (double)nRotationAngle.Value * Math.PI / 180.0;
                            UtilsConfigurations.RotationImage(ref temp, center, angle);
                        }
                        if (cTransform.Checked)
                        {
                            UtilsConfigurations.ImageTransformation(ref temp, (int)nudTransformX.Value, (int)nudTransformY.Value);
                        }
                        if (cFlip.Checked)
                        {
                            if(rbFlipX.Checked)
                            {
                                CvInvoke.Flip(temp, temp, Emgu.CV.CvEnum.FlipType.Horizontal);
                            }
                            else
                            {
                                CvInvoke.Flip(temp, temp, Emgu.CV.CvEnum.FlipType.Vertical);
                            }
                        }
                        if (cResize.Checked)
                        {
                            CvInvoke.Resize(temp, temp, new Size((int)nudResizeX.Value, (int)nudResizeY.Value));
                        }
                        imgBoxConfiguration.Image = temp;
                    }
                }
                catch { }
                
                this.Update();
            }));

        }
        private void rbNormal_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImageSource;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void rbGray_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImageGray;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void rbThreshold_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImageThreshold;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
            nudThreshold.Enabled = rbThreshold.Checked;
        }

        private void rThresholdInv_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImageThresholdInv;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
            nudThresholdInv.Enabled = rThresholdInv.Checked;
        }

        private void rbThresholdGaussian_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImageThresholdGaussian;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
            nudThresholdGaussian.Enabled = rbThresholdGaussian.Checked;
            nudCThresholdGaussian.Enabled = rbThresholdGaussian.Checked;
        }

        private void rbThresholdOstu_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImageThresholdOstu;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void rbLapalacian_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImageLaplacian;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
            nudKSizeLaplacian.Enabled  = rbLapalacian.Checked;
        }

        private void rbSobelX_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImageSobelX;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
            nudKSizeSobelX.Enabled = rbSobelX.Checked;
        }

        private void rbSobelY_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImageSobelY;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
            nudKSizeSobelY.Enabled = rbSobelY.Checked;
        }

        private void rbCanny_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImageCanny;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
            nudCannyMin.Enabled = nudCannyMax.Enabled = rbCanny.Checked;
        }

        private void nudThreshold_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.Threshold = Convert.ToInt32(nudThreshold.Value);
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void nudThresholdInv_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.ThresholdInv = Convert.ToInt32(nudThresholdInv.Value);
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void nudKSizeSobelX_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.KSobelX = Convert.ToInt32(nudKSizeSobelX.Value);
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }
        private void nudKSizeLaplacian_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.KLaplacian = Convert.ToInt32(nudKSizeLaplacian.Value);
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }
        private void nudKSizeSobelY_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.KSobelY = Convert.ToInt32(nudKSizeSobelY.Value);
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void nudCannyMin_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.MinCanny = Convert.ToInt32(nudCannyMin.Value);
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void nudCannyMax_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.MaxCanny = Convert.ToInt32(nudCannyMax.Value);
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btConnect_Click(object sender, EventArgs e)
        {
            if (btConnect.Text == "Connect")
            {
                if (cbListCamera.SelectedIndex != -1)
                {
                    string device = cbListCamera.SelectedItem.ToString();
                    int res = mCamera.OpenCamera_NET(device);
                    if (res == 0)
                    {
                        res = mCamera.StartGrabbing_NET();
                        if (res == 0)
                        {
                            int width = 0, height = 0;
                            mCamera.GetParameter_NET("Width", ref width, SetDataType.Integer);
                            mCamera.GetParameter_NET("Height", ref height, SetDataType.Integer);
                            lbShape.Text = string.Format("{0}x{1} Pixel", width, height);
                            mTimerIsRun = true;
                            mTimer.Enabled = true;
                            mTimer.Elapsed += OntimedEvent;
                            btConnect.Text = "Disconnect";
                        }
                        else
                        {
                            mCamera.CloseCamera_NET();
                            MessageBox.Show("Tao đell bật nổi Grabbing, Mã lỗi là 0x" + res.ToString("X8"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Tao đell mở nổi cái camera này, Mã lỗi là 0x" + res.ToString("X8"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Chọn Camera đi mày! \nKhông có camera bố tao xử lý được à?", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                mTimerIsRun = false;
                mTimer.Enabled = false;
                mCamera.CloseCamera_NET();
                btConnect.Text = "Connect";
            }

        }       
        private void cRotation_CheckedChanged(object sender, EventArgs e)
        {
            bool mode = cRotation.Checked;
            nRotationAngle.Enabled = mode;
            nRotationCenterX.Enabled = mode;
            nRotationCenterY.Enabled = mode;
            if (!mTimerIsRun)
            {
                ShowImg();
            }
        }
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            mTimerIsRun = false;
            mTimer.Enabled = false;
            if(mCamera.IsOpen)
            {
                mCamera.CloseCamera_NET();
            }
        }

        private void nRotationCenterX_ValueChanged(object sender, EventArgs e)
        {
            if (!mTimerIsRun)
            {
                ShowImg();
            }
        }

        private void nRotationCenterY_ValueChanged(object sender, EventArgs e)
        {
            if (!mTimerIsRun)
            {
                ShowImg();
            }
        }

        private void nRotationAngle_ValueChanged(object sender, EventArgs e)
        {
            if (!mTimerIsRun)
            {
                ShowImg();
            }
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            using (AboutBox about = new AboutBox())
            {
                about.ShowDialog();
            }
        }

        private void exitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cameraSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if(!mCamera.IsOpen)
            {
                using (CameraSettingsForm form = new CameraSettingsForm())
                {
                    form.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Ngắt kết nối với Camera đi đã!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }   
        }

        private void lightSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (LightSourceSettingsForm form = new LightSourceSettingsForm())
            {
                form.ShowDialog();
            }
        }
        private  void ColorSpaceChanged()
        {
            int h = Convert.ToInt32(lbHValue.Text);
            int s = Convert.ToInt32(lbSValue.Text);
            int v = Convert.ToInt32(lbVValue.Text);
            using (Image<Hsv, byte> ImageSRange = new Image<Hsv, byte>(12, 272, new Hsv(0, 255, 255)))
            {
                for (int y   = 0; y < 272; y++)
                {
                    double sVal = (double)(y * 255) / 272;
                    for (int x = 0; x < 12; x++)
                    {
                        ImageSRange[y, x] = new Hsv(h, Math.Abs(255- sVal), 255);
                    }
                }
                this.Invoke(new Action(() => {
                    pbSRange.Image = ImageSRange.Bitmap;
                }));
            }
            using (Image<Hsv, byte> ImageVRange = new Image<Hsv, byte>(12, 272, new Hsv(0, 255, 255)))
            {
                for (int y = 0; y < 272; y++)
                {
                    double vVal = (double)(y * 255) / 272;
                    for (int x = 0; x < 12; x++)
                    {
                        ImageVRange[y, x] = new Hsv(h, 255, Math.Abs(255 - vVal));
                    }
                }
                this.Invoke(new Action(() => {
                    pbVRange.Image = ImageVRange.Bitmap;
                }));
            }
            using (Image<Hsv, byte> ImageHSV = new Image<Hsv, byte>(127, 272, new Hsv(h, s, v)))
            {
                this.Invoke(new Action(() => {
                    pbHSV.Image = ImageHSV.Bitmap;
                }));
                
            }
        }

        private void trbH_Scroll(object sender, EventArgs e)
        {
            lbHValue.Text = trbH.Value.ToString();
            ColorSpaceChanged();
        }

        private void trbS_Scroll(object sender, EventArgs e)
        {
            lbSValue.Text = trbS.Value.ToString();
            ColorSpaceChanged();
        }

        private void trbV_Scroll(object sender, EventArgs e)
        {
            lbVValue.Text = trbV.Value.ToString();
            ColorSpaceChanged();
        }

        private void nudThresholdGaussian_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.ThresholdGaussian = Convert.ToInt32(nudThresholdGaussian.Value);
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void rbThresholdMean_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImageThresholdMean;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
            nudThresholdMean.Enabled = rbThresholdMean.Checked;
            nudCThresholdMean.Enabled = rbThresholdMean.Checked;
        }

        private void nudBlur_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.Blur = Convert.ToInt32(nudBlur.Value);
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void nudBlurGaussian_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.BlurGaussian = Convert.ToInt32(nudBlurGaussian.Value);
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void nudMedian_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.BlurMedian = Convert.ToInt32(nudBlurMedian.Value);
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void rbBlur_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImageBlur;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
            nudBlur.Enabled = rbBlur.Checked;
        }

        private void rbBlurGaussian_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImageBlurGaussian;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
            nudBlurGaussian.Enabled = rbBlurGaussian.Checked;
        }

        private void rbBlurMedian_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImageBlurMedian;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
            nudBlurMedian.Enabled = rbBlurMedian.Checked;
        }

        private void nudThresholdMean_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.ThresholdMean = Convert.ToInt32(nudThresholdMean.Value);
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void btKeepChanged_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Lưu thay đổi không chàng trai???", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(result == DialogResult.Yes)
            {
                Image<Bgr, byte> temp = null;
                switch (mMode)
                {
                    case modeProcessing.ImageSource:
                        if (UtilsConfigurations.ImageSource == null)
                            break;
                        temp = UtilsConfigurations.ImageSource.Copy();
                        break;
                    case modeProcessing.ImageGray:
                        if (UtilsConfigurations.ImageGray == null)
                            break;
                        temp = UtilsConfigurations.ImageGray.Convert<Bgr, byte>().Copy();
                        break;
                    case modeProcessing.ImageBlur:
                        if (UtilsConfigurations.ImageBlur == null)
                            break;
                        temp = UtilsConfigurations.ImageBlur.Copy();
                        break;
                    case modeProcessing.ImageBlurGaussian:
                        if (UtilsConfigurations.ImageBlurGaussian == null)
                            break;
                        temp = UtilsConfigurations.ImageBlurGaussian.Copy();
                        break;
                    case modeProcessing.ImageBlurMedian:
                        if (UtilsConfigurations.ImageBlurMedian == null)
                            break;
                        temp = UtilsConfigurations.ImageBlurMedian.Copy();
                        break;
                    case modeProcessing.ImageThreshold:
                        if (UtilsConfigurations.ImageThreshold == null)
                            break;
                        temp = UtilsConfigurations.ImageThreshold.Convert<Bgr, byte>().Copy();
                        break;

                    case modeProcessing.ImageThresholdInv:
                        if (UtilsConfigurations.ImageThresholdInv == null)
                            break;
                        temp = UtilsConfigurations.ImageThresholdInv.Convert<Bgr, byte>().Copy();
                        break;
                    case modeProcessing.ImageThresholdGaussian:
                        if (UtilsConfigurations.ImageThresholdGaussian == null)
                            break;
                        temp = UtilsConfigurations.ImageThresholdGaussian.Convert<Bgr, byte>().Copy();
                        break;
                    case modeProcessing.ImageThresholdMean:
                        if (UtilsConfigurations.ImageThresholdMean == null)
                            break;
                        temp = UtilsConfigurations.ImageThresholdMean.Convert<Bgr, byte>().Copy();
                        break;
                    case modeProcessing.ImageThresholdOstu:
                        if (UtilsConfigurations.ImageThresholdOstu == null)
                            break;
                        temp = UtilsConfigurations.ImageThresholdOstu.Convert<Bgr, byte>().Copy();
                        break;
                    case modeProcessing.ImageLaplacian:
                        if (UtilsConfigurations.ImageLaplacian == null)
                            break;
                        temp = UtilsConfigurations.ImageLaplacian.Convert<Bgr, byte>().Copy();
                        break;
                    case modeProcessing.ImageSobelX:
                        if (UtilsConfigurations.ImageSobelX == null)
                            break;
                        temp = UtilsConfigurations.ImageSobelX.Convert<Bgr, byte>().Copy();
                        break;
                    case modeProcessing.ImageSobelY:
                        if (UtilsConfigurations.ImageSobelY == null)
                            break;
                        temp = UtilsConfigurations.ImageSobelY.Convert<Bgr, byte>().Copy();
                        break;
                    case modeProcessing.ImageCanny:
                        if (UtilsConfigurations.ImageCanny == null)
                            break;
                        temp = UtilsConfigurations.ImageCanny.Convert<Bgr, byte>().Copy();
                        break;
                    case modeProcessing.ImageDilate:
                        if (UtilsConfigurations.ImageDilate == null)
                            break;
                        temp = UtilsConfigurations.ImageDilate.Convert<Bgr, byte>().Copy();
                        break;
                    case modeProcessing.ImageErode:
                        if (UtilsConfigurations.ImageErode == null)
                            break;
                        temp = UtilsConfigurations.ImageErode.Convert<Bgr, byte>().Copy();
                        break;
                    case modeProcessing.ImageOpen:
                        if (UtilsConfigurations.ImageOpen == null)
                            break;
                        temp = UtilsConfigurations.ImageOpen.Convert<Bgr, byte>().Copy();
                        break;
                    case modeProcessing.ImageClose:
                        if (UtilsConfigurations.ImageClose == null)
                            break;
                        temp = UtilsConfigurations.ImageClose.Convert<Bgr, byte>().Copy();
                        break;
                    default:
                        break;
                }
                if (temp != null)
                {
                    if (cRotation.Checked)
                    {
                        Point center = new Point((int)nRotationCenterX.Value, (int)nRotationCenterY.Value);
                        double angle = (double)nRotationAngle.Value * Math.PI / 180.0;
                        UtilsConfigurations.RotationImage(ref temp, center, angle);
                    }
                    if(UtilsConfigurations.ImageSource != null)
                    {
                        UtilsConfigurations.ImageSource.Dispose();
                        UtilsConfigurations.ImageSource = null;
                    }
                    UtilsConfigurations.ImageSource = temp.Copy();
                    rbNormal.Checked = true;
                }
            }
        }

        private void rbDilate_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImageDilate;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
            nudDilateX.Enabled = nudDilateY.Enabled = rbDilate.Checked;
        }

        private void rbErode_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImageErode;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
            nudErodeX.Enabled = nudErodeY.Enabled = rbErode.Checked;
        }

        private void rbOpen_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImageOpen;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
            nudOpenX.Enabled = nudOpenY.Enabled = rbOpen.Checked;
        }

        private void rbClose_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImageClose;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
            nudCloseX.Enabled = nudCloseY.Enabled = rbClose.Checked;
        }

        private void nudDilateX_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.DilateKernel = new Size(Convert.ToInt32(nudDilateX.Value), Convert.ToInt32(nudDilateY.Value));
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void nudDilateY_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.DilateKernel = new Size(Convert.ToInt32(nudDilateX.Value), Convert.ToInt32(nudDilateY.Value));
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void nudErodeX_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.ErodeKernel = new Size(Convert.ToInt32(nudErodeX.Value), Convert.ToInt32(nudErodeY.Value));
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void nudErodeY_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.ErodeKernel = new Size(Convert.ToInt32(nudErodeX.Value), Convert.ToInt32(nudErodeY.Value));
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void nudOpenX_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.OpenKernel = new Size(Convert.ToInt32(nudOpenX.Value), Convert.ToInt32(nudOpenY.Value));
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void nudOpenY_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.OpenKernel = new Size(Convert.ToInt32(nudOpenX.Value), Convert.ToInt32(nudOpenY.Value));
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void nudCloseX_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.CloseKernel = new Size(Convert.ToInt32(nudCloseX.Value), Convert.ToInt32(nudCloseY.Value));
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void nudCloseY_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.CloseKernel = new Size(Convert.ToInt32(nudCloseX.Value), Convert.ToInt32(nudCloseY.Value));
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (!mCamera.IsOpen)
            {
                using (CameraSettingsForm form = new CameraSettingsForm())
                {
                    form.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Ngắt kết nối với Camera đi đã!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            using (LightSourceSettingsForm form = new LightSourceSettingsForm())
            {
                form.ShowDialog();
            }
        }

        private void rbEqualization_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImgEqualization;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void nudCThresholdGaussian_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.CThresholdGaussian = Convert.ToInt32(nudCThresholdGaussian.Value);
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void nudCThresholdMean_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.CThresholdMean = Convert.ToInt32(nudCThresholdMean.Value);
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }


        private void nudDBilateral_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.DBilateral = Convert.ToInt32(nudDBilateral.Value);
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void nudsigmaColorBilateral_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.SigmaColorBilateral = Convert.ToInt32(nudsigmaColorBilateral.Value);
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void nudSigmaSpaceBilateral_ValueChanged(object sender, EventArgs e)
        {
            UtilsConfigurations.SigmaSpaceBilateral = Convert.ToInt32(nudSigmaSpaceBilateral.Value);
            UtilsConfigurations.Processing(mMode);
            ShowImg();
        }

        private void rbBilateralFilter_CheckedChanged(object sender, EventArgs e)
        {
            mMode = modeProcessing.ImgBilateral;
            UtilsConfigurations.Processing(mMode);
            ShowImg();
            nudDBilateral.Enabled = nudsigmaColorBilateral.Enabled = nudSigmaSpaceBilateral.Enabled = rbBilateralFilter.Checked;
        }

        private void cTransform_CheckedChanged(object sender, EventArgs e)
        {
            nudTransformX.Enabled = nudTransformY.Enabled = cTransform.Checked;
            if (!mTimerIsRun)
            {
                ShowImg();
            }
        }

        private void cFlip_CheckedChanged(object sender, EventArgs e)
        {
            rbFlipX.Enabled = rbFlipY.Enabled = cFlip.Checked;
            if (!mTimerIsRun)
            {
                ShowImg();
            }
        }

        private void cResize_CheckedChanged(object sender, EventArgs e)
        {
            nudResizeX.Enabled = nudResizeY.Enabled = cResize.Checked;
            if (!mTimerIsRun)
            {
                ShowImg();
            }
        }

        private void nudTransformX_ValueChanged(object sender, EventArgs e)
        {
            if (!mTimerIsRun)
            {
                ShowImg();
            }
        }

        private void rbFlipY_CheckedChanged(object sender, EventArgs e)
        {
            if (!mTimerIsRun)
            {
                ShowImg();
            }
        }

        private void nudTransformY_ValueChanged(object sender, EventArgs e)
        {
            if (!mTimerIsRun)
            {
                ShowImg();
            }
        }

        private void rbFlipX_CheckedChanged(object sender, EventArgs e)
        {
            if (!mTimerIsRun)
            {
                ShowImg();
            }
        }

        private void nudResizeX_ValueChanged(object sender, EventArgs e)
        {
            if (!mTimerIsRun)
            {
                ShowImg();
            }
        }

        private void nudResizeY_ValueChanged(object sender, EventArgs e)
        {
            if (!mTimerIsRun)
            {
                ShowImg();
            }
        }
    }
}
