﻿namespace Heal
{
    partial class LightSourceSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LightSourceSettingsForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nChannel4 = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.nChannel3 = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.nChannel2 = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.nChannel1 = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.nBaud = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbPort = new System.Windows.Forms.ComboBox();
            this.btClose = new System.Windows.Forms.Button();
            this.btOpen = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nChannel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nChannel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nChannel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nChannel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nBaud)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nChannel4);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.nChannel3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.nChannel2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.nChannel1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.nBaud);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cbPort);
            this.groupBox1.Controls.Add(this.btClose);
            this.groupBox1.Controls.Add(this.btOpen);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(316, 128);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // nChannel4
            // 
            this.nChannel4.Location = new System.Drawing.Point(250, 93);
            this.nChannel4.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nChannel4.Name = "nChannel4";
            this.nChannel4.Size = new System.Drawing.Size(47, 20);
            this.nChannel4.TabIndex = 13;
            this.nChannel4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nChannel4.Value = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.nChannel4.ValueChanged += new System.EventHandler(this.nChannel4_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(186, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Channel 4:";
            // 
            // nChannel3
            // 
            this.nChannel3.Location = new System.Drawing.Point(250, 67);
            this.nChannel3.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nChannel3.Name = "nChannel3";
            this.nChannel3.Size = new System.Drawing.Size(47, 20);
            this.nChannel3.TabIndex = 11;
            this.nChannel3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nChannel3.Value = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.nChannel3.ValueChanged += new System.EventHandler(this.nChannel3_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(186, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Channel 3:";
            // 
            // nChannel2
            // 
            this.nChannel2.Location = new System.Drawing.Point(250, 41);
            this.nChannel2.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nChannel2.Name = "nChannel2";
            this.nChannel2.Size = new System.Drawing.Size(47, 20);
            this.nChannel2.TabIndex = 9;
            this.nChannel2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nChannel2.Value = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.nChannel2.ValueChanged += new System.EventHandler(this.nChannel2_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(186, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Channel 2:";
            // 
            // nChannel1
            // 
            this.nChannel1.Location = new System.Drawing.Point(250, 15);
            this.nChannel1.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nChannel1.Name = "nChannel1";
            this.nChannel1.Size = new System.Drawing.Size(47, 20);
            this.nChannel1.TabIndex = 7;
            this.nChannel1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nChannel1.Value = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.nChannel1.ValueChanged += new System.EventHandler(this.nChannel1_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(186, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Channel 1:";
            // 
            // nBaud
            // 
            this.nBaud.Location = new System.Drawing.Point(57, 53);
            this.nBaud.Maximum = new decimal(new int[] {
            115200,
            0,
            0,
            0});
            this.nBaud.Minimum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.nBaud.Name = "nBaud";
            this.nBaud.Size = new System.Drawing.Size(92, 20);
            this.nBaud.TabIndex = 5;
            this.nBaud.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nBaud.Value = new decimal(new int[] {
            19200,
            0,
            0,
            0});
            this.nBaud.ValueChanged += new System.EventHandler(this.nBaud_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Baud:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Port:";
            // 
            // cbPort
            // 
            this.cbPort.FormattingEnabled = true;
            this.cbPort.Location = new System.Drawing.Point(57, 19);
            this.cbPort.Name = "cbPort";
            this.cbPort.Size = new System.Drawing.Size(92, 21);
            this.cbPort.TabIndex = 2;
            this.cbPort.SelectedIndexChanged += new System.EventHandler(this.cbPort_SelectedIndexChanged);
            // 
            // btClose
            // 
            this.btClose.Location = new System.Drawing.Point(87, 88);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(62, 23);
            this.btClose.TabIndex = 1;
            this.btClose.Text = "Close";
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // btOpen
            // 
            this.btOpen.Location = new System.Drawing.Point(19, 88);
            this.btOpen.Name = "btOpen";
            this.btOpen.Size = new System.Drawing.Size(62, 23);
            this.btOpen.TabIndex = 0;
            this.btOpen.Text = "Open";
            this.btOpen.UseVisualStyleBackColor = true;
            this.btOpen.Click += new System.EventHandler(this.btOpen_Click);
            // 
            // LightSourceSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(342, 149);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(358, 188);
            this.Name = "LightSourceSettingsForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Light Source Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LightSourceSettingsForm_FormClosing);
            this.Load += new System.EventHandler(this.LightSourceSettingsForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nChannel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nChannel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nChannel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nChannel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nBaud)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btClose;
        private System.Windows.Forms.Button btOpen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbPort;
        private System.Windows.Forms.NumericUpDown nBaud;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nChannel4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nChannel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nChannel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nChannel1;
        private System.Windows.Forms.Label label3;
    }
}