﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Heal
{
    public partial class CameraSettingsForm : Form
    {
        private HikVisionCameraSDK mCamera = HikVisionCameraSDK.Instance;
        public CameraSettingsForm()
        {
            InitializeComponent();
        }

        private void btReloadCamera_Click(object sender, EventArgs e)
        {
            string oldSelection = string.Empty;
            string[] device = mCamera.GetAllCameraName_NET();
            if (cbListCamera.SelectedIndex != -1)
            {
                cbListCamera.SelectedItem.ToString();
            }
            cbListCamera.Items.Clear();
            if (device != null)
            {
                cbListCamera.Items.AddRange(device);
                if (device.Contains(oldSelection))
                {
                    cbListCamera.SelectedItem = oldSelection;
                }
                else
                {
                    cbListCamera.SelectedIndex = 0;
                }
            }
        }

        private void btOpenCamera_Click(object sender, EventArgs e)
        {
            if (cbListCamera.SelectedIndex != -1)
            {
                string device = cbListCamera.SelectedItem.ToString();
                int res = mCamera.OpenCamera_NET(device);
                if (res == 0)
                {
                    res = mCamera.StartGrabbing_NET();
                    if (res == 0)
                    {
                        mCamera.Display_NET(imageBox.Handle);
                        updateUI(true);
                    }
                    else
                    {
                        mCamera.CloseCamera_NET();
                        MessageBox.Show("Tao đell bật nổi Grabbing, Mã lỗi là 0x" + res.ToString("X8"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Tao đell mở nổi cái camera này, Mã lỗi là 0x" + res.ToString("X8"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Chọn Camera đi mày! \nKhông có camera bố tao xử lý được à?", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btCloseCamera_Click(object sender, EventArgs e)
        {
            int res = mCamera.CloseCamera_NET();
            if(res == 0)
            {
                updateUI(false);
                imageBox.Image = null;
            }
            else
            {
                MessageBox.Show("Tao đell tắt nổi cái camera này, Mã lỗi là 0x" + res.ToString("X8"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void updateUI(bool mode)
        {
            btOpenCamera.Enabled = !mode;
            btReloadCamera.Enabled = !mode;
            btCloseCamera.Enabled = mode;
            panelSettings.Enabled = mode;
            if(mode)
            {
                float exposureTime = 0;
                int res = mCamera.GetParameter_NET("ExposureTime", ref exposureTime, SetDataType.Float);
                if(res == 0)
                {
                    nExposureTime.Value = Convert.ToInt32( exposureTime);
                }
                else
                {
                    MessageBox.Show("Tao đell lấy được giá trị phơi sáng của cái camera này, Mã lỗi là 0x" + res.ToString("X8"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        private void CameraSettingsForm_Load(object sender, EventArgs e)
        {
            updateUI(false);
            btReloadCamera_Click(null, null);
        }

        private void nExposureTime_ValueChanged(object sender, EventArgs e)
        {
            int value = Convert.ToInt32(nExposureTime.Value);
            int res = mCamera.SetParameter_NET("ExposureTime", value , SetDataType.Float);
            if (res != 0)
            {
                MessageBox.Show("Tao đell set được giá trị phơi sáng của cái camera này, Mã lỗi là 0x" + res.ToString("X8"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btBrowser_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "Configuration file | *.mfs";
                if(ofd.ShowDialog() == DialogResult.OK)
                {
                    txtPathSettingsFile.Text = ofd.FileName;

                    bool isGrabbing = mCamera.IsGrab;
                    if(mCamera.IsOpen)
                    {
                        mCamera.StopGrabbing_NET();
                        bool res = mCamera.FeatureLoad_NET(ofd.FileName);
                        if(res)
                        {
                            MessageBox.Show("Thành công rồi! Mọi thứ đã được cập nhật!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Tao đell load nổi cái file cài đặt này, Mã lỗi là 0x" + (-13265).ToString("X8"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        if (isGrabbing)
                            mCamera.StartGrabbing_NET();
                        float exposureTime = 0;
                        int res1 = mCamera.GetParameter_NET("ExposureTime", ref exposureTime, SetDataType.Float);
                        if (res1 == 0)
                        {
                            nExposureTime.Value = Convert.ToInt32(exposureTime);
                        }
                        else
                        {
                            MessageBox.Show("Tao đell lấy được giá trị phơi sáng của cái camera này, Mã lỗi là 0x" + res1.ToString("X8"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
        }

        private void CameraSettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            mCamera.CloseCamera_NET();
        }
    }
}
