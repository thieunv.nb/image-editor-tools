﻿namespace Heal
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.actionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cameraSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lightSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabCustomization = new System.Windows.Forms.TabPage();
            this.grConfigurations = new System.Windows.Forms.GroupBox();
            this.rbFromCamera = new System.Windows.Forms.RadioButton();
            this.rbFromFile = new System.Windows.Forms.RadioButton();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rbEqualization = new System.Windows.Forms.RadioButton();
            this.nudDBilateral = new System.Windows.Forms.NumericUpDown();
            this.nudsigmaColorBilateral = new System.Windows.Forms.NumericUpDown();
            this.nudSigmaSpaceBilateral = new System.Windows.Forms.NumericUpDown();
            this.rbBilateralFilter = new System.Windows.Forms.RadioButton();
            this.nudCThresholdGaussian = new System.Windows.Forms.NumericUpDown();
            this.nudCThresholdMean = new System.Windows.Forms.NumericUpDown();
            this.nudCloseX = new System.Windows.Forms.NumericUpDown();
            this.nudCloseY = new System.Windows.Forms.NumericUpDown();
            this.rbClose = new System.Windows.Forms.RadioButton();
            this.nudOpenX = new System.Windows.Forms.NumericUpDown();
            this.nudOpenY = new System.Windows.Forms.NumericUpDown();
            this.rbOpen = new System.Windows.Forms.RadioButton();
            this.nudErodeX = new System.Windows.Forms.NumericUpDown();
            this.nudErodeY = new System.Windows.Forms.NumericUpDown();
            this.rbErode = new System.Windows.Forms.RadioButton();
            this.nudDilateX = new System.Windows.Forms.NumericUpDown();
            this.nudDilateY = new System.Windows.Forms.NumericUpDown();
            this.rbDilate = new System.Windows.Forms.RadioButton();
            this.nudBlurMedian = new System.Windows.Forms.NumericUpDown();
            this.rbBlurMedian = new System.Windows.Forms.RadioButton();
            this.nudBlurGaussian = new System.Windows.Forms.NumericUpDown();
            this.rbBlurGaussian = new System.Windows.Forms.RadioButton();
            this.nudBlur = new System.Windows.Forms.NumericUpDown();
            this.rbBlur = new System.Windows.Forms.RadioButton();
            this.nudThresholdMean = new System.Windows.Forms.NumericUpDown();
            this.rbThresholdMean = new System.Windows.Forms.RadioButton();
            this.nudThresholdGaussian = new System.Windows.Forms.NumericUpDown();
            this.lbShape = new System.Windows.Forms.Label();
            this.nudKSizeLaplacian = new System.Windows.Forms.NumericUpDown();
            this.nudCannyMin = new System.Windows.Forms.NumericUpDown();
            this.nudCannyMax = new System.Windows.Forms.NumericUpDown();
            this.nudKSizeSobelY = new System.Windows.Forms.NumericUpDown();
            this.nudKSizeSobelX = new System.Windows.Forms.NumericUpDown();
            this.nudThresholdInv = new System.Windows.Forms.NumericUpDown();
            this.nudThreshold = new System.Windows.Forms.NumericUpDown();
            this.rbGray = new System.Windows.Forms.RadioButton();
            this.rbCanny = new System.Windows.Forms.RadioButton();
            this.rbSobelY = new System.Windows.Forms.RadioButton();
            this.rbSobelX = new System.Windows.Forms.RadioButton();
            this.rbLapalacian = new System.Windows.Forms.RadioButton();
            this.rbThresholdOstu = new System.Windows.Forms.RadioButton();
            this.rbThresholdGaussian = new System.Windows.Forms.RadioButton();
            this.rThresholdInv = new System.Windows.Forms.RadioButton();
            this.rbThreshold = new System.Windows.Forms.RadioButton();
            this.rbNormal = new System.Windows.Forms.RadioButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.rbFlipY = new System.Windows.Forms.RadioButton();
            this.rbFlipX = new System.Windows.Forms.RadioButton();
            this.cFlip = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.nudResizeX = new System.Windows.Forms.NumericUpDown();
            this.cResize = new System.Windows.Forms.CheckBox();
            this.nudResizeY = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.nudTransformX = new System.Windows.Forms.NumericUpDown();
            this.cTransform = new System.Windows.Forms.CheckBox();
            this.nudTransformY = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.nRotationCenterY = new System.Windows.Forms.NumericUpDown();
            this.nRotationCenterX = new System.Windows.Forms.NumericUpDown();
            this.nRotationAngle = new System.Windows.Forms.NumericUpDown();
            this.cRotation = new System.Windows.Forms.CheckBox();
            this.btKeepChanged = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btBrowser = new System.Windows.Forms.Button();
            this.btConnect = new System.Windows.Forms.Button();
            this.cbListCamera = new System.Windows.Forms.ComboBox();
            this.imgBoxConfiguration = new Emgu.CV.UI.ImageBox();
            this.tabExtractByColorSpace = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pbVRange = new System.Windows.Forms.PictureBox();
            this.trbV = new System.Windows.Forms.TrackBar();
            this.lbVValue = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pbSRange = new System.Windows.Forms.PictureBox();
            this.trbS = new System.Windows.Forms.TrackBar();
            this.lbSValue = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pbHSV = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.trbH = new System.Windows.Forms.TrackBar();
            this.lbHValue = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabCustomization.SuspendLayout();
            this.grConfigurations.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDBilateral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudsigmaColorBilateral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSigmaSpaceBilateral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCThresholdGaussian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCThresholdMean)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCloseX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCloseY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOpenX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOpenY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudErodeX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudErodeY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDilateX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDilateY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBlurMedian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBlurGaussian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBlur)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThresholdMean)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThresholdGaussian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudKSizeLaplacian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCannyMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCannyMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudKSizeSobelY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudKSizeSobelX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThresholdInv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThreshold)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudResizeX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudResizeY)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTransformX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTransformY)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nRotationCenterY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nRotationCenterX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nRotationAngle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBoxConfiguration)).BeginInit();
            this.tabExtractByColorSpace.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbVRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbV)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbS)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHSV)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbH)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.actionToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1074, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // actionToolStripMenuItem
            // 
            this.actionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cameraSettingsToolStripMenuItem,
            this.lightSettingsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.actionToolStripMenuItem.Name = "actionToolStripMenuItem";
            this.actionToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.actionToolStripMenuItem.Text = "Action";
            // 
            // cameraSettingsToolStripMenuItem
            // 
            this.cameraSettingsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cameraSettingsToolStripMenuItem.Image")));
            this.cameraSettingsToolStripMenuItem.Name = "cameraSettingsToolStripMenuItem";
            this.cameraSettingsToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.cameraSettingsToolStripMenuItem.Text = "Camera Settings";
            this.cameraSettingsToolStripMenuItem.Click += new System.EventHandler(this.cameraSettingsToolStripMenuItem_Click);
            // 
            // lightSettingsToolStripMenuItem
            // 
            this.lightSettingsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("lightSettingsToolStripMenuItem.Image")));
            this.lightSettingsToolStripMenuItem.Name = "lightSettingsToolStripMenuItem";
            this.lightSettingsToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.lightSettingsToolStripMenuItem.Text = "Light Settings";
            this.lightSettingsToolStripMenuItem.Click += new System.EventHandler(this.lightSettingsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exitToolStripMenuItem.Image")));
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click_1);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem1});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.aboutToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem1
            // 
            this.aboutToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("aboutToolStripMenuItem1.Image")));
            this.aboutToolStripMenuItem1.Name = "aboutToolStripMenuItem1";
            this.aboutToolStripMenuItem1.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem1.Text = "About";
            this.aboutToolStripMenuItem1.Click += new System.EventHandler(this.aboutToolStripMenuItem1_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabCustomization);
            this.tabControl1.Controls.Add(this.tabExtractByColorSpace);
            this.tabControl1.ItemSize = new System.Drawing.Size(120, 20);
            this.tabControl1.Location = new System.Drawing.Point(12, 55);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(3, 3);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1050, 648);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 1;
            // 
            // tabCustomization
            // 
            this.tabCustomization.Controls.Add(this.grConfigurations);
            this.tabCustomization.Controls.Add(this.imgBoxConfiguration);
            this.tabCustomization.Location = new System.Drawing.Point(4, 24);
            this.tabCustomization.Name = "tabCustomization";
            this.tabCustomization.Padding = new System.Windows.Forms.Padding(3);
            this.tabCustomization.Size = new System.Drawing.Size(1042, 620);
            this.tabCustomization.TabIndex = 0;
            this.tabCustomization.Text = "Customization";
            this.tabCustomization.UseVisualStyleBackColor = true;
            // 
            // grConfigurations
            // 
            this.grConfigurations.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grConfigurations.Controls.Add(this.rbFromCamera);
            this.grConfigurations.Controls.Add(this.rbFromFile);
            this.grConfigurations.Controls.Add(this.tabControl2);
            this.grConfigurations.Controls.Add(this.btKeepChanged);
            this.grConfigurations.Controls.Add(this.label2);
            this.grConfigurations.Controls.Add(this.txtFilePath);
            this.grConfigurations.Controls.Add(this.label1);
            this.grConfigurations.Controls.Add(this.btBrowser);
            this.grConfigurations.Controls.Add(this.btConnect);
            this.grConfigurations.Controls.Add(this.cbListCamera);
            this.grConfigurations.Location = new System.Drawing.Point(781, 3);
            this.grConfigurations.Name = "grConfigurations";
            this.grConfigurations.Size = new System.Drawing.Size(253, 611);
            this.grConfigurations.TabIndex = 3;
            this.grConfigurations.TabStop = false;
            this.grConfigurations.Text = "Mode";
            // 
            // rbFromCamera
            // 
            this.rbFromCamera.AutoSize = true;
            this.rbFromCamera.Checked = true;
            this.rbFromCamera.Location = new System.Drawing.Point(12, 19);
            this.rbFromCamera.Name = "rbFromCamera";
            this.rbFromCamera.Size = new System.Drawing.Size(87, 17);
            this.rbFromCamera.TabIndex = 0;
            this.rbFromCamera.TabStop = true;
            this.rbFromCamera.Text = "From Camera";
            this.rbFromCamera.UseVisualStyleBackColor = true;
            this.rbFromCamera.CheckedChanged += new System.EventHandler(this.rbFromCamera_CheckedChanged);
            // 
            // rbFromFile
            // 
            this.rbFromFile.AutoSize = true;
            this.rbFromFile.Location = new System.Drawing.Point(148, 19);
            this.rbFromFile.Name = "rbFromFile";
            this.rbFromFile.Size = new System.Drawing.Size(67, 17);
            this.rbFromFile.TabIndex = 1;
            this.rbFromFile.Text = "From File";
            this.rbFromFile.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Location = new System.Drawing.Point(0, 130);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(255, 449);
            this.tabControl2.TabIndex = 43;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(247, 423);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Featured";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rbEqualization);
            this.groupBox4.Controls.Add(this.nudDBilateral);
            this.groupBox4.Controls.Add(this.nudsigmaColorBilateral);
            this.groupBox4.Controls.Add(this.nudSigmaSpaceBilateral);
            this.groupBox4.Controls.Add(this.rbBilateralFilter);
            this.groupBox4.Controls.Add(this.nudCThresholdGaussian);
            this.groupBox4.Controls.Add(this.nudCThresholdMean);
            this.groupBox4.Controls.Add(this.nudCloseX);
            this.groupBox4.Controls.Add(this.nudCloseY);
            this.groupBox4.Controls.Add(this.rbClose);
            this.groupBox4.Controls.Add(this.nudOpenX);
            this.groupBox4.Controls.Add(this.nudOpenY);
            this.groupBox4.Controls.Add(this.rbOpen);
            this.groupBox4.Controls.Add(this.nudErodeX);
            this.groupBox4.Controls.Add(this.nudErodeY);
            this.groupBox4.Controls.Add(this.rbErode);
            this.groupBox4.Controls.Add(this.nudDilateX);
            this.groupBox4.Controls.Add(this.nudDilateY);
            this.groupBox4.Controls.Add(this.rbDilate);
            this.groupBox4.Controls.Add(this.nudBlurMedian);
            this.groupBox4.Controls.Add(this.rbBlurMedian);
            this.groupBox4.Controls.Add(this.nudBlurGaussian);
            this.groupBox4.Controls.Add(this.rbBlurGaussian);
            this.groupBox4.Controls.Add(this.nudBlur);
            this.groupBox4.Controls.Add(this.rbBlur);
            this.groupBox4.Controls.Add(this.nudThresholdMean);
            this.groupBox4.Controls.Add(this.rbThresholdMean);
            this.groupBox4.Controls.Add(this.nudThresholdGaussian);
            this.groupBox4.Controls.Add(this.lbShape);
            this.groupBox4.Controls.Add(this.nudKSizeLaplacian);
            this.groupBox4.Controls.Add(this.nudCannyMin);
            this.groupBox4.Controls.Add(this.nudCannyMax);
            this.groupBox4.Controls.Add(this.nudKSizeSobelY);
            this.groupBox4.Controls.Add(this.nudKSizeSobelX);
            this.groupBox4.Controls.Add(this.nudThresholdInv);
            this.groupBox4.Controls.Add(this.nudThreshold);
            this.groupBox4.Controls.Add(this.rbGray);
            this.groupBox4.Controls.Add(this.rbCanny);
            this.groupBox4.Controls.Add(this.rbSobelY);
            this.groupBox4.Controls.Add(this.rbSobelX);
            this.groupBox4.Controls.Add(this.rbLapalacian);
            this.groupBox4.Controls.Add(this.rbThresholdOstu);
            this.groupBox4.Controls.Add(this.rbThresholdGaussian);
            this.groupBox4.Controls.Add(this.rThresholdInv);
            this.groupBox4.Controls.Add(this.rbThreshold);
            this.groupBox4.Controls.Add(this.rbNormal);
            this.groupBox4.Location = new System.Drawing.Point(5, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(238, 427);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            // 
            // rbEqualization
            // 
            this.rbEqualization.AutoSize = true;
            this.rbEqualization.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbEqualization.Location = new System.Drawing.Point(16, 31);
            this.rbEqualization.Name = "rbEqualization";
            this.rbEqualization.Size = new System.Drawing.Size(82, 17);
            this.rbEqualization.TabIndex = 60;
            this.rbEqualization.Text = "Equalization";
            this.rbEqualization.UseVisualStyleBackColor = true;
            this.rbEqualization.CheckedChanged += new System.EventHandler(this.rbEqualization_CheckedChanged);
            // 
            // nudDBilateral
            // 
            this.nudDBilateral.Enabled = false;
            this.nudDBilateral.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudDBilateral.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudDBilateral.Location = new System.Drawing.Point(101, 128);
            this.nudDBilateral.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nudDBilateral.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudDBilateral.Name = "nudDBilateral";
            this.nudDBilateral.Size = new System.Drawing.Size(43, 20);
            this.nudDBilateral.TabIndex = 59;
            this.nudDBilateral.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudDBilateral.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.nudDBilateral.ValueChanged += new System.EventHandler(this.nudDBilateral_ValueChanged);
            // 
            // nudsigmaColorBilateral
            // 
            this.nudsigmaColorBilateral.Enabled = false;
            this.nudsigmaColorBilateral.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudsigmaColorBilateral.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudsigmaColorBilateral.Location = new System.Drawing.Point(145, 128);
            this.nudsigmaColorBilateral.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nudsigmaColorBilateral.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudsigmaColorBilateral.Name = "nudsigmaColorBilateral";
            this.nudsigmaColorBilateral.Size = new System.Drawing.Size(43, 20);
            this.nudsigmaColorBilateral.TabIndex = 58;
            this.nudsigmaColorBilateral.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudsigmaColorBilateral.Value = new decimal(new int[] {
            11,
            0,
            0,
            0});
            this.nudsigmaColorBilateral.ValueChanged += new System.EventHandler(this.nudsigmaColorBilateral_ValueChanged);
            // 
            // nudSigmaSpaceBilateral
            // 
            this.nudSigmaSpaceBilateral.Enabled = false;
            this.nudSigmaSpaceBilateral.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudSigmaSpaceBilateral.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudSigmaSpaceBilateral.Location = new System.Drawing.Point(189, 128);
            this.nudSigmaSpaceBilateral.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nudSigmaSpaceBilateral.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudSigmaSpaceBilateral.Name = "nudSigmaSpaceBilateral";
            this.nudSigmaSpaceBilateral.Size = new System.Drawing.Size(43, 20);
            this.nudSigmaSpaceBilateral.TabIndex = 57;
            this.nudSigmaSpaceBilateral.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudSigmaSpaceBilateral.Value = new decimal(new int[] {
            11,
            0,
            0,
            0});
            this.nudSigmaSpaceBilateral.ValueChanged += new System.EventHandler(this.nudSigmaSpaceBilateral_ValueChanged);
            // 
            // rbBilateralFilter
            // 
            this.rbBilateralFilter.AutoSize = true;
            this.rbBilateralFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbBilateralFilter.Location = new System.Drawing.Point(16, 128);
            this.rbBilateralFilter.Name = "rbBilateralFilter";
            this.rbBilateralFilter.Size = new System.Drawing.Size(87, 17);
            this.rbBilateralFilter.TabIndex = 56;
            this.rbBilateralFilter.Text = "Bilateral Filter";
            this.rbBilateralFilter.UseVisualStyleBackColor = true;
            this.rbBilateralFilter.CheckedChanged += new System.EventHandler(this.rbBilateralFilter_CheckedChanged);
            // 
            // nudCThresholdGaussian
            // 
            this.nudCThresholdGaussian.Enabled = false;
            this.nudCThresholdGaussian.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudCThresholdGaussian.Location = new System.Drawing.Point(189, 188);
            this.nudCThresholdGaussian.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudCThresholdGaussian.Name = "nudCThresholdGaussian";
            this.nudCThresholdGaussian.Size = new System.Drawing.Size(43, 20);
            this.nudCThresholdGaussian.TabIndex = 55;
            this.nudCThresholdGaussian.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudCThresholdGaussian.ValueChanged += new System.EventHandler(this.nudCThresholdGaussian_ValueChanged);
            // 
            // nudCThresholdMean
            // 
            this.nudCThresholdMean.Enabled = false;
            this.nudCThresholdMean.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudCThresholdMean.Location = new System.Drawing.Point(189, 208);
            this.nudCThresholdMean.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudCThresholdMean.Name = "nudCThresholdMean";
            this.nudCThresholdMean.Size = new System.Drawing.Size(43, 20);
            this.nudCThresholdMean.TabIndex = 54;
            this.nudCThresholdMean.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudCThresholdMean.ValueChanged += new System.EventHandler(this.nudCThresholdMean_ValueChanged);
            // 
            // nudCloseX
            // 
            this.nudCloseX.Enabled = false;
            this.nudCloseX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudCloseX.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudCloseX.Location = new System.Drawing.Point(145, 308);
            this.nudCloseX.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nudCloseX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCloseX.Name = "nudCloseX";
            this.nudCloseX.Size = new System.Drawing.Size(43, 20);
            this.nudCloseX.TabIndex = 53;
            this.nudCloseX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudCloseX.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudCloseX.ValueChanged += new System.EventHandler(this.nudCloseX_ValueChanged);
            // 
            // nudCloseY
            // 
            this.nudCloseY.Enabled = false;
            this.nudCloseY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudCloseY.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudCloseY.Location = new System.Drawing.Point(189, 308);
            this.nudCloseY.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nudCloseY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCloseY.Name = "nudCloseY";
            this.nudCloseY.Size = new System.Drawing.Size(43, 20);
            this.nudCloseY.TabIndex = 52;
            this.nudCloseY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudCloseY.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudCloseY.ValueChanged += new System.EventHandler(this.nudCloseY_ValueChanged);
            // 
            // rbClose
            // 
            this.rbClose.AutoSize = true;
            this.rbClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbClose.Location = new System.Drawing.Point(16, 308);
            this.rbClose.Name = "rbClose";
            this.rbClose.Size = new System.Drawing.Size(51, 17);
            this.rbClose.TabIndex = 51;
            this.rbClose.Text = "Close";
            this.rbClose.UseVisualStyleBackColor = true;
            this.rbClose.CheckedChanged += new System.EventHandler(this.rbClose_CheckedChanged);
            // 
            // nudOpenX
            // 
            this.nudOpenX.Enabled = false;
            this.nudOpenX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudOpenX.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudOpenX.Location = new System.Drawing.Point(145, 288);
            this.nudOpenX.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nudOpenX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudOpenX.Name = "nudOpenX";
            this.nudOpenX.Size = new System.Drawing.Size(43, 20);
            this.nudOpenX.TabIndex = 50;
            this.nudOpenX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudOpenX.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudOpenX.ValueChanged += new System.EventHandler(this.nudOpenX_ValueChanged);
            // 
            // nudOpenY
            // 
            this.nudOpenY.Enabled = false;
            this.nudOpenY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudOpenY.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudOpenY.Location = new System.Drawing.Point(189, 288);
            this.nudOpenY.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nudOpenY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudOpenY.Name = "nudOpenY";
            this.nudOpenY.Size = new System.Drawing.Size(43, 20);
            this.nudOpenY.TabIndex = 49;
            this.nudOpenY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudOpenY.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudOpenY.ValueChanged += new System.EventHandler(this.nudOpenY_ValueChanged);
            // 
            // rbOpen
            // 
            this.rbOpen.AutoSize = true;
            this.rbOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbOpen.Location = new System.Drawing.Point(16, 288);
            this.rbOpen.Name = "rbOpen";
            this.rbOpen.Size = new System.Drawing.Size(51, 17);
            this.rbOpen.TabIndex = 48;
            this.rbOpen.Text = "Open";
            this.rbOpen.UseVisualStyleBackColor = true;
            this.rbOpen.CheckedChanged += new System.EventHandler(this.rbOpen_CheckedChanged);
            // 
            // nudErodeX
            // 
            this.nudErodeX.Enabled = false;
            this.nudErodeX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudErodeX.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudErodeX.Location = new System.Drawing.Point(145, 268);
            this.nudErodeX.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nudErodeX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudErodeX.Name = "nudErodeX";
            this.nudErodeX.Size = new System.Drawing.Size(43, 20);
            this.nudErodeX.TabIndex = 47;
            this.nudErodeX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudErodeX.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudErodeX.ValueChanged += new System.EventHandler(this.nudErodeX_ValueChanged);
            // 
            // nudErodeY
            // 
            this.nudErodeY.Enabled = false;
            this.nudErodeY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudErodeY.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudErodeY.Location = new System.Drawing.Point(189, 268);
            this.nudErodeY.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nudErodeY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudErodeY.Name = "nudErodeY";
            this.nudErodeY.Size = new System.Drawing.Size(43, 20);
            this.nudErodeY.TabIndex = 46;
            this.nudErodeY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudErodeY.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudErodeY.ValueChanged += new System.EventHandler(this.nudErodeY_ValueChanged);
            // 
            // rbErode
            // 
            this.rbErode.AutoSize = true;
            this.rbErode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbErode.Location = new System.Drawing.Point(16, 268);
            this.rbErode.Name = "rbErode";
            this.rbErode.Size = new System.Drawing.Size(53, 17);
            this.rbErode.TabIndex = 45;
            this.rbErode.Text = "Erode";
            this.rbErode.UseVisualStyleBackColor = true;
            this.rbErode.CheckedChanged += new System.EventHandler(this.rbErode_CheckedChanged);
            // 
            // nudDilateX
            // 
            this.nudDilateX.Enabled = false;
            this.nudDilateX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudDilateX.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudDilateX.Location = new System.Drawing.Point(145, 248);
            this.nudDilateX.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nudDilateX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudDilateX.Name = "nudDilateX";
            this.nudDilateX.Size = new System.Drawing.Size(43, 20);
            this.nudDilateX.TabIndex = 44;
            this.nudDilateX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudDilateX.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudDilateX.ValueChanged += new System.EventHandler(this.nudDilateX_ValueChanged);
            // 
            // nudDilateY
            // 
            this.nudDilateY.Enabled = false;
            this.nudDilateY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudDilateY.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudDilateY.Location = new System.Drawing.Point(189, 248);
            this.nudDilateY.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nudDilateY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudDilateY.Name = "nudDilateY";
            this.nudDilateY.Size = new System.Drawing.Size(43, 20);
            this.nudDilateY.TabIndex = 43;
            this.nudDilateY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudDilateY.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudDilateY.ValueChanged += new System.EventHandler(this.nudDilateY_ValueChanged);
            // 
            // rbDilate
            // 
            this.rbDilate.AutoSize = true;
            this.rbDilate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbDilate.Location = new System.Drawing.Point(16, 248);
            this.rbDilate.Name = "rbDilate";
            this.rbDilate.Size = new System.Drawing.Size(52, 17);
            this.rbDilate.TabIndex = 42;
            this.rbDilate.Text = "Dilate";
            this.rbDilate.UseVisualStyleBackColor = true;
            this.rbDilate.CheckedChanged += new System.EventHandler(this.rbDilate_CheckedChanged);
            // 
            // nudBlurMedian
            // 
            this.nudBlurMedian.Enabled = false;
            this.nudBlurMedian.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudBlurMedian.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudBlurMedian.Location = new System.Drawing.Point(189, 108);
            this.nudBlurMedian.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nudBlurMedian.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudBlurMedian.Name = "nudBlurMedian";
            this.nudBlurMedian.Size = new System.Drawing.Size(43, 20);
            this.nudBlurMedian.TabIndex = 41;
            this.nudBlurMedian.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudBlurMedian.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudBlurMedian.ValueChanged += new System.EventHandler(this.nudMedian_ValueChanged);
            // 
            // rbBlurMedian
            // 
            this.rbBlurMedian.AutoSize = true;
            this.rbBlurMedian.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbBlurMedian.Location = new System.Drawing.Point(16, 108);
            this.rbBlurMedian.Name = "rbBlurMedian";
            this.rbBlurMedian.Size = new System.Drawing.Size(81, 17);
            this.rbBlurMedian.TabIndex = 40;
            this.rbBlurMedian.Text = "Blur Median";
            this.rbBlurMedian.UseVisualStyleBackColor = true;
            this.rbBlurMedian.CheckedChanged += new System.EventHandler(this.rbBlurMedian_CheckedChanged);
            // 
            // nudBlurGaussian
            // 
            this.nudBlurGaussian.Enabled = false;
            this.nudBlurGaussian.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudBlurGaussian.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudBlurGaussian.Location = new System.Drawing.Point(189, 88);
            this.nudBlurGaussian.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nudBlurGaussian.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudBlurGaussian.Name = "nudBlurGaussian";
            this.nudBlurGaussian.Size = new System.Drawing.Size(43, 20);
            this.nudBlurGaussian.TabIndex = 39;
            this.nudBlurGaussian.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudBlurGaussian.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudBlurGaussian.ValueChanged += new System.EventHandler(this.nudBlurGaussian_ValueChanged);
            // 
            // rbBlurGaussian
            // 
            this.rbBlurGaussian.AutoSize = true;
            this.rbBlurGaussian.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbBlurGaussian.Location = new System.Drawing.Point(16, 91);
            this.rbBlurGaussian.Name = "rbBlurGaussian";
            this.rbBlurGaussian.Size = new System.Drawing.Size(90, 17);
            this.rbBlurGaussian.TabIndex = 38;
            this.rbBlurGaussian.Text = "Blur Gaussian";
            this.rbBlurGaussian.UseVisualStyleBackColor = true;
            this.rbBlurGaussian.CheckedChanged += new System.EventHandler(this.rbBlurGaussian_CheckedChanged);
            // 
            // nudBlur
            // 
            this.nudBlur.Enabled = false;
            this.nudBlur.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudBlur.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudBlur.Location = new System.Drawing.Point(189, 68);
            this.nudBlur.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nudBlur.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudBlur.Name = "nudBlur";
            this.nudBlur.Size = new System.Drawing.Size(43, 20);
            this.nudBlur.TabIndex = 37;
            this.nudBlur.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudBlur.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudBlur.ValueChanged += new System.EventHandler(this.nudBlur_ValueChanged);
            // 
            // rbBlur
            // 
            this.rbBlur.AutoSize = true;
            this.rbBlur.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbBlur.Location = new System.Drawing.Point(16, 71);
            this.rbBlur.Name = "rbBlur";
            this.rbBlur.Size = new System.Drawing.Size(43, 17);
            this.rbBlur.TabIndex = 36;
            this.rbBlur.Text = "Blur";
            this.rbBlur.UseVisualStyleBackColor = true;
            this.rbBlur.CheckedChanged += new System.EventHandler(this.rbBlur_CheckedChanged);
            // 
            // nudThresholdMean
            // 
            this.nudThresholdMean.Enabled = false;
            this.nudThresholdMean.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudThresholdMean.Location = new System.Drawing.Point(145, 208);
            this.nudThresholdMean.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nudThresholdMean.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudThresholdMean.Name = "nudThresholdMean";
            this.nudThresholdMean.Size = new System.Drawing.Size(43, 20);
            this.nudThresholdMean.TabIndex = 35;
            this.nudThresholdMean.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudThresholdMean.Value = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.nudThresholdMean.ValueChanged += new System.EventHandler(this.nudThresholdMean_ValueChanged);
            // 
            // rbThresholdMean
            // 
            this.rbThresholdMean.AutoSize = true;
            this.rbThresholdMean.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbThresholdMean.Location = new System.Drawing.Point(16, 208);
            this.rbThresholdMean.Name = "rbThresholdMean";
            this.rbThresholdMean.Size = new System.Drawing.Size(102, 17);
            this.rbThresholdMean.TabIndex = 34;
            this.rbThresholdMean.Text = "Threshold Mean";
            this.rbThresholdMean.UseVisualStyleBackColor = true;
            this.rbThresholdMean.CheckedChanged += new System.EventHandler(this.rbThresholdMean_CheckedChanged);
            // 
            // nudThresholdGaussian
            // 
            this.nudThresholdGaussian.Enabled = false;
            this.nudThresholdGaussian.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudThresholdGaussian.Location = new System.Drawing.Point(145, 188);
            this.nudThresholdGaussian.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nudThresholdGaussian.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudThresholdGaussian.Name = "nudThresholdGaussian";
            this.nudThresholdGaussian.Size = new System.Drawing.Size(43, 20);
            this.nudThresholdGaussian.TabIndex = 33;
            this.nudThresholdGaussian.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudThresholdGaussian.Value = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.nudThresholdGaussian.ValueChanged += new System.EventHandler(this.nudThresholdGaussian_ValueChanged);
            // 
            // lbShape
            // 
            this.lbShape.Enabled = false;
            this.lbShape.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbShape.Location = new System.Drawing.Point(105, 13);
            this.lbShape.Name = "lbShape";
            this.lbShape.Size = new System.Drawing.Size(92, 13);
            this.lbShape.TabIndex = 32;
            this.lbShape.Text = "0x0";
            this.lbShape.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nudKSizeLaplacian
            // 
            this.nudKSizeLaplacian.Enabled = false;
            this.nudKSizeLaplacian.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudKSizeLaplacian.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudKSizeLaplacian.Location = new System.Drawing.Point(189, 328);
            this.nudKSizeLaplacian.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudKSizeLaplacian.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudKSizeLaplacian.Name = "nudKSizeLaplacian";
            this.nudKSizeLaplacian.Size = new System.Drawing.Size(43, 20);
            this.nudKSizeLaplacian.TabIndex = 16;
            this.nudKSizeLaplacian.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudKSizeLaplacian.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudKSizeLaplacian.ValueChanged += new System.EventHandler(this.nudKSizeLaplacian_ValueChanged);
            // 
            // nudCannyMin
            // 
            this.nudCannyMin.Enabled = false;
            this.nudCannyMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudCannyMin.Location = new System.Drawing.Point(145, 388);
            this.nudCannyMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudCannyMin.Name = "nudCannyMin";
            this.nudCannyMin.Size = new System.Drawing.Size(43, 20);
            this.nudCannyMin.TabIndex = 15;
            this.nudCannyMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudCannyMin.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudCannyMin.ValueChanged += new System.EventHandler(this.nudCannyMin_ValueChanged);
            // 
            // nudCannyMax
            // 
            this.nudCannyMax.Enabled = false;
            this.nudCannyMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudCannyMax.Location = new System.Drawing.Point(189, 388);
            this.nudCannyMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudCannyMax.Name = "nudCannyMax";
            this.nudCannyMax.Size = new System.Drawing.Size(43, 20);
            this.nudCannyMax.TabIndex = 14;
            this.nudCannyMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudCannyMax.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.nudCannyMax.ValueChanged += new System.EventHandler(this.nudCannyMax_ValueChanged);
            // 
            // nudKSizeSobelY
            // 
            this.nudKSizeSobelY.Enabled = false;
            this.nudKSizeSobelY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudKSizeSobelY.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudKSizeSobelY.Location = new System.Drawing.Point(189, 368);
            this.nudKSizeSobelY.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudKSizeSobelY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudKSizeSobelY.Name = "nudKSizeSobelY";
            this.nudKSizeSobelY.Size = new System.Drawing.Size(43, 20);
            this.nudKSizeSobelY.TabIndex = 13;
            this.nudKSizeSobelY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudKSizeSobelY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudKSizeSobelY.ValueChanged += new System.EventHandler(this.nudKSizeSobelY_ValueChanged);
            // 
            // nudKSizeSobelX
            // 
            this.nudKSizeSobelX.Enabled = false;
            this.nudKSizeSobelX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudKSizeSobelX.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudKSizeSobelX.Location = new System.Drawing.Point(189, 348);
            this.nudKSizeSobelX.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudKSizeSobelX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudKSizeSobelX.Name = "nudKSizeSobelX";
            this.nudKSizeSobelX.Size = new System.Drawing.Size(43, 20);
            this.nudKSizeSobelX.TabIndex = 12;
            this.nudKSizeSobelX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudKSizeSobelX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudKSizeSobelX.ValueChanged += new System.EventHandler(this.nudKSizeSobelX_ValueChanged);
            // 
            // nudThresholdInv
            // 
            this.nudThresholdInv.Enabled = false;
            this.nudThresholdInv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudThresholdInv.Location = new System.Drawing.Point(189, 168);
            this.nudThresholdInv.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudThresholdInv.Name = "nudThresholdInv";
            this.nudThresholdInv.Size = new System.Drawing.Size(43, 20);
            this.nudThresholdInv.TabIndex = 11;
            this.nudThresholdInv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudThresholdInv.Value = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.nudThresholdInv.ValueChanged += new System.EventHandler(this.nudThresholdInv_ValueChanged);
            // 
            // nudThreshold
            // 
            this.nudThreshold.Enabled = false;
            this.nudThreshold.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudThreshold.Location = new System.Drawing.Point(189, 148);
            this.nudThreshold.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudThreshold.Name = "nudThreshold";
            this.nudThreshold.Size = new System.Drawing.Size(43, 20);
            this.nudThreshold.TabIndex = 10;
            this.nudThreshold.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudThreshold.Value = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.nudThreshold.ValueChanged += new System.EventHandler(this.nudThreshold_ValueChanged);
            // 
            // rbGray
            // 
            this.rbGray.AutoSize = true;
            this.rbGray.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbGray.Location = new System.Drawing.Point(16, 51);
            this.rbGray.Name = "rbGray";
            this.rbGray.Size = new System.Drawing.Size(47, 17);
            this.rbGray.TabIndex = 9;
            this.rbGray.Text = "Gray";
            this.rbGray.UseVisualStyleBackColor = true;
            this.rbGray.CheckedChanged += new System.EventHandler(this.rbGray_CheckedChanged);
            // 
            // rbCanny
            // 
            this.rbCanny.AutoSize = true;
            this.rbCanny.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbCanny.Location = new System.Drawing.Point(16, 388);
            this.rbCanny.Name = "rbCanny";
            this.rbCanny.Size = new System.Drawing.Size(55, 17);
            this.rbCanny.TabIndex = 8;
            this.rbCanny.Text = "Canny";
            this.rbCanny.UseVisualStyleBackColor = true;
            this.rbCanny.CheckedChanged += new System.EventHandler(this.rbCanny_CheckedChanged);
            // 
            // rbSobelY
            // 
            this.rbSobelY.AutoSize = true;
            this.rbSobelY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSobelY.Location = new System.Drawing.Point(16, 368);
            this.rbSobelY.Name = "rbSobelY";
            this.rbSobelY.Size = new System.Drawing.Size(62, 17);
            this.rbSobelY.TabIndex = 7;
            this.rbSobelY.Text = "Sobel Y";
            this.rbSobelY.UseVisualStyleBackColor = true;
            this.rbSobelY.CheckedChanged += new System.EventHandler(this.rbSobelY_CheckedChanged);
            // 
            // rbSobelX
            // 
            this.rbSobelX.AutoSize = true;
            this.rbSobelX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSobelX.Location = new System.Drawing.Point(16, 348);
            this.rbSobelX.Name = "rbSobelX";
            this.rbSobelX.Size = new System.Drawing.Size(62, 17);
            this.rbSobelX.TabIndex = 6;
            this.rbSobelX.Text = "Sobel X";
            this.rbSobelX.UseVisualStyleBackColor = true;
            this.rbSobelX.CheckedChanged += new System.EventHandler(this.rbSobelX_CheckedChanged);
            // 
            // rbLapalacian
            // 
            this.rbLapalacian.AutoSize = true;
            this.rbLapalacian.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbLapalacian.Location = new System.Drawing.Point(16, 328);
            this.rbLapalacian.Name = "rbLapalacian";
            this.rbLapalacian.Size = new System.Drawing.Size(71, 17);
            this.rbLapalacian.TabIndex = 5;
            this.rbLapalacian.Text = "Laplacian";
            this.rbLapalacian.UseVisualStyleBackColor = true;
            this.rbLapalacian.CheckedChanged += new System.EventHandler(this.rbLapalacian_CheckedChanged);
            // 
            // rbThresholdOstu
            // 
            this.rbThresholdOstu.AutoSize = true;
            this.rbThresholdOstu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbThresholdOstu.Location = new System.Drawing.Point(16, 228);
            this.rbThresholdOstu.Name = "rbThresholdOstu";
            this.rbThresholdOstu.Size = new System.Drawing.Size(97, 17);
            this.rbThresholdOstu.TabIndex = 4;
            this.rbThresholdOstu.Text = "Threshold Ostu";
            this.rbThresholdOstu.UseVisualStyleBackColor = true;
            this.rbThresholdOstu.CheckedChanged += new System.EventHandler(this.rbThresholdOstu_CheckedChanged);
            // 
            // rbThresholdGaussian
            // 
            this.rbThresholdGaussian.AutoSize = true;
            this.rbThresholdGaussian.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbThresholdGaussian.Location = new System.Drawing.Point(16, 188);
            this.rbThresholdGaussian.Name = "rbThresholdGaussian";
            this.rbThresholdGaussian.Size = new System.Drawing.Size(119, 17);
            this.rbThresholdGaussian.TabIndex = 3;
            this.rbThresholdGaussian.Text = "Threshold Gaussian";
            this.rbThresholdGaussian.UseVisualStyleBackColor = true;
            this.rbThresholdGaussian.CheckedChanged += new System.EventHandler(this.rbThresholdGaussian_CheckedChanged);
            // 
            // rThresholdInv
            // 
            this.rThresholdInv.AutoSize = true;
            this.rThresholdInv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rThresholdInv.Location = new System.Drawing.Point(16, 168);
            this.rThresholdInv.Name = "rThresholdInv";
            this.rThresholdInv.Size = new System.Drawing.Size(110, 17);
            this.rThresholdInv.TabIndex = 2;
            this.rThresholdInv.Text = "Threshold Inverse";
            this.rThresholdInv.UseVisualStyleBackColor = true;
            this.rThresholdInv.CheckedChanged += new System.EventHandler(this.rThresholdInv_CheckedChanged);
            // 
            // rbThreshold
            // 
            this.rbThreshold.AutoSize = true;
            this.rbThreshold.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbThreshold.Location = new System.Drawing.Point(16, 148);
            this.rbThreshold.Name = "rbThreshold";
            this.rbThreshold.Size = new System.Drawing.Size(72, 17);
            this.rbThreshold.TabIndex = 1;
            this.rbThreshold.Text = "Threshold";
            this.rbThreshold.UseVisualStyleBackColor = true;
            this.rbThreshold.CheckedChanged += new System.EventHandler(this.rbThreshold_CheckedChanged);
            // 
            // rbNormal
            // 
            this.rbNormal.AutoSize = true;
            this.rbNormal.Checked = true;
            this.rbNormal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNormal.Location = new System.Drawing.Point(16, 11);
            this.rbNormal.Name = "rbNormal";
            this.rbNormal.Size = new System.Drawing.Size(58, 17);
            this.rbNormal.TabIndex = 0;
            this.rbNormal.TabStop = true;
            this.rbNormal.Text = "Normal";
            this.rbNormal.UseVisualStyleBackColor = true;
            this.rbNormal.CheckedChanged += new System.EventHandler(this.rbNormal_CheckedChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(247, 423);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Geometric";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.rbFlipY);
            this.groupBox6.Controls.Add(this.rbFlipX);
            this.groupBox6.Controls.Add(this.cFlip);
            this.groupBox6.Location = new System.Drawing.Point(6, 171);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(234, 70);
            this.groupBox6.TabIndex = 33;
            this.groupBox6.TabStop = false;
            // 
            // rbFlipY
            // 
            this.rbFlipY.AutoSize = true;
            this.rbFlipY.Enabled = false;
            this.rbFlipY.Location = new System.Drawing.Point(103, 40);
            this.rbFlipY.Name = "rbFlipY";
            this.rbFlipY.Size = new System.Drawing.Size(77, 17);
            this.rbFlipY.TabIndex = 26;
            this.rbFlipY.Text = "Y Direction";
            this.rbFlipY.UseVisualStyleBackColor = true;
            this.rbFlipY.CheckedChanged += new System.EventHandler(this.rbFlipY_CheckedChanged);
            // 
            // rbFlipX
            // 
            this.rbFlipX.AutoSize = true;
            this.rbFlipX.Checked = true;
            this.rbFlipX.Enabled = false;
            this.rbFlipX.Location = new System.Drawing.Point(6, 40);
            this.rbFlipX.Name = "rbFlipX";
            this.rbFlipX.Size = new System.Drawing.Size(77, 17);
            this.rbFlipX.TabIndex = 25;
            this.rbFlipX.TabStop = true;
            this.rbFlipX.Text = "X Direction";
            this.rbFlipX.UseVisualStyleBackColor = true;
            this.rbFlipX.CheckedChanged += new System.EventHandler(this.rbFlipX_CheckedChanged);
            // 
            // cFlip
            // 
            this.cFlip.AutoSize = true;
            this.cFlip.Location = new System.Drawing.Point(5, 12);
            this.cFlip.Name = "cFlip";
            this.cFlip.Size = new System.Drawing.Size(42, 17);
            this.cFlip.TabIndex = 24;
            this.cFlip.Text = "Flip";
            this.cFlip.UseVisualStyleBackColor = true;
            this.cFlip.CheckedChanged += new System.EventHandler(this.cFlip_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.nudResizeX);
            this.groupBox5.Controls.Add(this.cResize);
            this.groupBox5.Controls.Add(this.nudResizeY);
            this.groupBox5.Location = new System.Drawing.Point(5, 247);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(234, 63);
            this.groupBox5.TabIndex = 32;
            this.groupBox5.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(103, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 13);
            this.label3.TabIndex = 29;
            this.label3.Text = "Y:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 37);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "X:";
            // 
            // nudResizeX
            // 
            this.nudResizeX.Enabled = false;
            this.nudResizeX.Location = new System.Drawing.Point(33, 35);
            this.nudResizeX.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudResizeX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudResizeX.Name = "nudResizeX";
            this.nudResizeX.Size = new System.Drawing.Size(43, 20);
            this.nudResizeX.TabIndex = 26;
            this.nudResizeX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudResizeX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudResizeX.ValueChanged += new System.EventHandler(this.nudResizeX_ValueChanged);
            // 
            // cResize
            // 
            this.cResize.AutoSize = true;
            this.cResize.Location = new System.Drawing.Point(5, 12);
            this.cResize.Name = "cResize";
            this.cResize.Size = new System.Drawing.Size(58, 17);
            this.cResize.TabIndex = 24;
            this.cResize.Text = "Resize";
            this.cResize.UseVisualStyleBackColor = true;
            this.cResize.CheckedChanged += new System.EventHandler(this.cResize_CheckedChanged);
            // 
            // nudResizeY
            // 
            this.nudResizeY.Enabled = false;
            this.nudResizeY.Location = new System.Drawing.Point(131, 35);
            this.nudResizeY.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudResizeY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudResizeY.Name = "nudResizeY";
            this.nudResizeY.Size = new System.Drawing.Size(43, 20);
            this.nudResizeY.TabIndex = 27;
            this.nudResizeY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudResizeY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudResizeY.ValueChanged += new System.EventHandler(this.nudResizeY_ValueChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.nudTransformX);
            this.groupBox3.Controls.Add(this.cTransform);
            this.groupBox3.Controls.Add(this.nudTransformY);
            this.groupBox3.Location = new System.Drawing.Point(6, 102);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(234, 63);
            this.groupBox3.TabIndex = 31;
            this.groupBox3.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(103, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "Y:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "X:";
            // 
            // nudTransformX
            // 
            this.nudTransformX.Enabled = false;
            this.nudTransformX.Location = new System.Drawing.Point(33, 35);
            this.nudTransformX.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudTransformX.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.nudTransformX.Name = "nudTransformX";
            this.nudTransformX.Size = new System.Drawing.Size(43, 20);
            this.nudTransformX.TabIndex = 26;
            this.nudTransformX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudTransformX.ValueChanged += new System.EventHandler(this.nudTransformX_ValueChanged);
            // 
            // cTransform
            // 
            this.cTransform.AutoSize = true;
            this.cTransform.Location = new System.Drawing.Point(5, 12);
            this.cTransform.Name = "cTransform";
            this.cTransform.Size = new System.Drawing.Size(73, 17);
            this.cTransform.TabIndex = 24;
            this.cTransform.Text = "Transform";
            this.cTransform.UseVisualStyleBackColor = true;
            this.cTransform.CheckedChanged += new System.EventHandler(this.cTransform_CheckedChanged);
            // 
            // nudTransformY
            // 
            this.nudTransformY.Enabled = false;
            this.nudTransformY.Location = new System.Drawing.Point(131, 35);
            this.nudTransformY.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudTransformY.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.nudTransformY.Name = "nudTransformY";
            this.nudTransformY.Size = new System.Drawing.Size(43, 20);
            this.nudTransformY.TabIndex = 27;
            this.nudTransformY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudTransformY.ValueChanged += new System.EventHandler(this.nudTransformY_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.nRotationCenterY);
            this.groupBox1.Controls.Add(this.nRotationCenterX);
            this.groupBox1.Controls.Add(this.nRotationAngle);
            this.groupBox1.Controls.Add(this.cRotation);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(234, 90);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(137, 37);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "Angle:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Center Y:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 37);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Center X:";
            // 
            // nRotationCenterY
            // 
            this.nRotationCenterY.Location = new System.Drawing.Point(64, 61);
            this.nRotationCenterY.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nRotationCenterY.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.nRotationCenterY.Name = "nRotationCenterY";
            this.nRotationCenterY.Size = new System.Drawing.Size(43, 20);
            this.nRotationCenterY.TabIndex = 27;
            this.nRotationCenterY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nRotationCenterY.ValueChanged += new System.EventHandler(this.nRotationCenterY_ValueChanged);
            // 
            // nRotationCenterX
            // 
            this.nRotationCenterX.Location = new System.Drawing.Point(63, 35);
            this.nRotationCenterX.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nRotationCenterX.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.nRotationCenterX.Name = "nRotationCenterX";
            this.nRotationCenterX.Size = new System.Drawing.Size(43, 20);
            this.nRotationCenterX.TabIndex = 26;
            this.nRotationCenterX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nRotationCenterX.ValueChanged += new System.EventHandler(this.nRotationCenterX_ValueChanged);
            // 
            // nRotationAngle
            // 
            this.nRotationAngle.Location = new System.Drawing.Point(185, 35);
            this.nRotationAngle.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nRotationAngle.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nRotationAngle.Name = "nRotationAngle";
            this.nRotationAngle.Size = new System.Drawing.Size(43, 20);
            this.nRotationAngle.TabIndex = 25;
            this.nRotationAngle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nRotationAngle.ValueChanged += new System.EventHandler(this.nRotationAngle_ValueChanged);
            // 
            // cRotation
            // 
            this.cRotation.AutoSize = true;
            this.cRotation.Location = new System.Drawing.Point(5, 12);
            this.cRotation.Name = "cRotation";
            this.cRotation.Size = new System.Drawing.Size(66, 17);
            this.cRotation.TabIndex = 24;
            this.cRotation.Text = "Rotation";
            this.cRotation.UseVisualStyleBackColor = true;
            this.cRotation.CheckedChanged += new System.EventHandler(this.cRotation_CheckedChanged);
            // 
            // btKeepChanged
            // 
            this.btKeepChanged.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btKeepChanged.Location = new System.Drawing.Point(148, 582);
            this.btKeepChanged.Name = "btKeepChanged";
            this.btKeepChanged.Size = new System.Drawing.Size(96, 23);
            this.btKeepChanged.TabIndex = 42;
            this.btKeepChanged.Text = "Keep Changed";
            this.btKeepChanged.UseVisualStyleBackColor = true;
            this.btKeepChanged.Click += new System.EventHandler(this.btKeepChanged_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(196, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Browser:";
            // 
            // txtFilePath
            // 
            this.txtFilePath.BackColor = System.Drawing.SystemColors.Menu;
            this.txtFilePath.Location = new System.Drawing.Point(12, 104);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(203, 20);
            this.txtFilePath.TabIndex = 12;
            this.txtFilePath.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(158, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Camera (Reload):";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // btBrowser
            // 
            this.btBrowser.Location = new System.Drawing.Point(221, 102);
            this.btBrowser.Name = "btBrowser";
            this.btBrowser.Size = new System.Drawing.Size(26, 23);
            this.btBrowser.TabIndex = 11;
            this.btBrowser.TabStop = false;
            this.btBrowser.Text = "...";
            this.btBrowser.UseVisualStyleBackColor = true;
            this.btBrowser.Click += new System.EventHandler(this.btBrowser_Click);
            // 
            // btConnect
            // 
            this.btConnect.Location = new System.Drawing.Point(176, 58);
            this.btConnect.Name = "btConnect";
            this.btConnect.Size = new System.Drawing.Size(71, 23);
            this.btConnect.TabIndex = 10;
            this.btConnect.TabStop = false;
            this.btConnect.Text = "Connect";
            this.btConnect.UseVisualStyleBackColor = true;
            this.btConnect.Click += new System.EventHandler(this.btConnect_Click);
            // 
            // cbListCamera
            // 
            this.cbListCamera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbListCamera.FormattingEnabled = true;
            this.cbListCamera.Location = new System.Drawing.Point(9, 59);
            this.cbListCamera.Name = "cbListCamera";
            this.cbListCamera.Size = new System.Drawing.Size(161, 21);
            this.cbListCamera.TabIndex = 7;
            this.cbListCamera.TabStop = false;
            // 
            // imgBoxConfiguration
            // 
            this.imgBoxConfiguration.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.imgBoxConfiguration.BackColor = System.Drawing.Color.Gainsboro;
            this.imgBoxConfiguration.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgBoxConfiguration.Location = new System.Drawing.Point(8, 6);
            this.imgBoxConfiguration.Name = "imgBoxConfiguration";
            this.imgBoxConfiguration.Size = new System.Drawing.Size(767, 608);
            this.imgBoxConfiguration.TabIndex = 2;
            this.imgBoxConfiguration.TabStop = false;
            // 
            // tabExtractByColorSpace
            // 
            this.tabExtractByColorSpace.Controls.Add(this.groupBox2);
            this.tabExtractByColorSpace.Location = new System.Drawing.Point(4, 24);
            this.tabExtractByColorSpace.Name = "tabExtractByColorSpace";
            this.tabExtractByColorSpace.Size = new System.Drawing.Size(1042, 620);
            this.tabExtractByColorSpace.TabIndex = 1;
            this.tabExtractByColorSpace.Text = "Extract by Color Space";
            this.tabExtractByColorSpace.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel6);
            this.groupBox2.Controls.Add(this.panel5);
            this.groupBox2.Controls.Add(this.panel2);
            this.groupBox2.Controls.Add(this.panel4);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox2.Location = new System.Drawing.Point(8, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(344, 362);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "HSV Space Color";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.pbVRange);
            this.panel6.Controls.Add(this.trbV);
            this.panel6.Controls.Add(this.lbVValue);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Location = new System.Drawing.Point(154, 19);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(68, 333);
            this.panel6.TabIndex = 10;
            // 
            // pbVRange
            // 
            this.pbVRange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbVRange.Location = new System.Drawing.Point(45, 33);
            this.pbVRange.Name = "pbVRange";
            this.pbVRange.Size = new System.Drawing.Size(12, 272);
            this.pbVRange.TabIndex = 11;
            this.pbVRange.TabStop = false;
            // 
            // trbV
            // 
            this.trbV.Location = new System.Drawing.Point(12, 20);
            this.trbV.Maximum = 255;
            this.trbV.Name = "trbV";
            this.trbV.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trbV.Size = new System.Drawing.Size(45, 295);
            this.trbV.TabIndex = 8;
            this.trbV.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trbV.Value = 255;
            this.trbV.Scroll += new System.EventHandler(this.trbV_Scroll);
            // 
            // lbVValue
            // 
            this.lbVValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVValue.ForeColor = System.Drawing.Color.Crimson;
            this.lbVValue.Location = new System.Drawing.Point(12, 7);
            this.lbVValue.Name = "lbVValue";
            this.lbVValue.Size = new System.Drawing.Size(45, 23);
            this.lbVValue.TabIndex = 7;
            this.lbVValue.Text = "255";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(42, 314);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(18, 16);
            this.label14.TabIndex = 6;
            this.label14.Text = "V";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.pbSRange);
            this.panel5.Controls.Add(this.trbS);
            this.panel5.Controls.Add(this.lbSValue);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Location = new System.Drawing.Point(80, 19);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(68, 333);
            this.panel5.TabIndex = 9;
            // 
            // pbSRange
            // 
            this.pbSRange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbSRange.Location = new System.Drawing.Point(45, 33);
            this.pbSRange.Name = "pbSRange";
            this.pbSRange.Size = new System.Drawing.Size(12, 272);
            this.pbSRange.TabIndex = 10;
            this.pbSRange.TabStop = false;
            // 
            // trbS
            // 
            this.trbS.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trbS.Location = new System.Drawing.Point(12, 20);
            this.trbS.Maximum = 255;
            this.trbS.Name = "trbS";
            this.trbS.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trbS.Size = new System.Drawing.Size(45, 295);
            this.trbS.TabIndex = 8;
            this.trbS.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trbS.Value = 255;
            this.trbS.Scroll += new System.EventHandler(this.trbS_Scroll);
            // 
            // lbSValue
            // 
            this.lbSValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSValue.ForeColor = System.Drawing.Color.Crimson;
            this.lbSValue.Location = new System.Drawing.Point(12, 7);
            this.lbSValue.Name = "lbSValue";
            this.lbSValue.Size = new System.Drawing.Size(45, 23);
            this.lbSValue.TabIndex = 7;
            this.lbSValue.Text = "255";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(42, 314);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(18, 16);
            this.label12.TabIndex = 6;
            this.label12.Text = "S";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pbHSV);
            this.panel2.Location = new System.Drawing.Point(228, 19);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(102, 330);
            this.panel2.TabIndex = 3;
            // 
            // pbHSV
            // 
            this.pbHSV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbHSV.Location = new System.Drawing.Point(30, 33);
            this.pbHSV.Name = "pbHSV";
            this.pbHSV.Size = new System.Drawing.Size(41, 272);
            this.pbHSV.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbHSV.TabIndex = 0;
            this.pbHSV.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Controls.Add(this.trbH);
            this.panel4.Controls.Add(this.lbHValue);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Location = new System.Drawing.Point(6, 19);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(68, 333);
            this.panel4.TabIndex = 8;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(45, 33);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(12, 272);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // trbH
            // 
            this.trbH.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trbH.Location = new System.Drawing.Point(12, 20);
            this.trbH.Maximum = 179;
            this.trbH.Name = "trbH";
            this.trbH.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trbH.Size = new System.Drawing.Size(45, 295);
            this.trbH.TabIndex = 8;
            this.trbH.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trbH.Scroll += new System.EventHandler(this.trbH_Scroll);
            // 
            // lbHValue
            // 
            this.lbHValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHValue.ForeColor = System.Drawing.Color.Crimson;
            this.lbHValue.Location = new System.Drawing.Point(12, 7);
            this.lbHValue.Name = "lbHValue";
            this.lbHValue.Size = new System.Drawing.Size(45, 23);
            this.lbHValue.TabIndex = 7;
            this.lbHValue.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(42, 314);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(19, 16);
            this.label6.TabIndex = 6;
            this.label6.Text = "H";
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripSeparator1,
            this.toolStripButton2,
            this.toolStripSeparator2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1074, 28);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.AutoSize = false;
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(26, 26);
            this.toolStripButton1.Text = "HIK Camera Controller";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.AutoSize = false;
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(27, 27);
            this.toolStripButton2.Text = "Light Source Controller";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1074, 715);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(1090, 754);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Image Processing Tools";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabCustomization.ResumeLayout(false);
            this.grConfigurations.ResumeLayout(false);
            this.grConfigurations.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDBilateral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudsigmaColorBilateral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSigmaSpaceBilateral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCThresholdGaussian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCThresholdMean)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCloseX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCloseY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOpenX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOpenY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudErodeX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudErodeY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDilateX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDilateY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBlurMedian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBlurGaussian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBlur)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThresholdMean)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThresholdGaussian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudKSizeLaplacian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCannyMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCannyMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudKSizeSobelY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudKSizeSobelX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThresholdInv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThreshold)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudResizeX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudResizeY)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTransformX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTransformY)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nRotationCenterY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nRotationCenterX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nRotationAngle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBoxConfiguration)).EndInit();
            this.tabExtractByColorSpace.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbVRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbV)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbS)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbHSV)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbH)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem actionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cameraSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lightSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabCustomization;
        private Emgu.CV.UI.ImageBox imgBoxConfiguration;
        private System.Windows.Forms.GroupBox grConfigurations;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown nudCannyMin;
        private System.Windows.Forms.NumericUpDown nudCannyMax;
        private System.Windows.Forms.NumericUpDown nudKSizeSobelY;
        private System.Windows.Forms.NumericUpDown nudKSizeSobelX;
        private System.Windows.Forms.NumericUpDown nudThresholdInv;
        private System.Windows.Forms.NumericUpDown nudThreshold;
        private System.Windows.Forms.RadioButton rbGray;
        private System.Windows.Forms.RadioButton rbCanny;
        private System.Windows.Forms.RadioButton rbSobelY;
        private System.Windows.Forms.RadioButton rbSobelX;
        private System.Windows.Forms.RadioButton rbLapalacian;
        private System.Windows.Forms.RadioButton rbThresholdOstu;
        private System.Windows.Forms.RadioButton rbThresholdGaussian;
        private System.Windows.Forms.RadioButton rThresholdInv;
        private System.Windows.Forms.RadioButton rbThreshold;
        private System.Windows.Forms.RadioButton rbNormal;
        private System.Windows.Forms.TextBox txtFilePath;
        private System.Windows.Forms.Button btBrowser;
        private System.Windows.Forms.Button btConnect;
        private System.Windows.Forms.RadioButton rbFromFile;
        private System.Windows.Forms.RadioButton rbFromCamera;
        private System.Windows.Forms.ComboBox cbListCamera;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown nRotationAngle;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown nRotationCenterY;
        private System.Windows.Forms.NumericUpDown nRotationCenterX;
        private System.Windows.Forms.CheckBox cRotation;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem1;
        private System.Windows.Forms.NumericUpDown nudKSizeLaplacian;
        private System.Windows.Forms.Label lbShape;
        private System.Windows.Forms.TabPage tabExtractByColorSpace;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pbHSV;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lbVValue;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lbSValue;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lbHValue;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TrackBar trbV;
        private System.Windows.Forms.TrackBar trbS;
        private System.Windows.Forms.TrackBar trbH;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pbVRange;
        private System.Windows.Forms.PictureBox pbSRange;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.NumericUpDown nudThresholdGaussian;
        private System.Windows.Forms.NumericUpDown nudThresholdMean;
        private System.Windows.Forms.RadioButton rbThresholdMean;
        private System.Windows.Forms.NumericUpDown nudBlurMedian;
        private System.Windows.Forms.RadioButton rbBlurMedian;
        private System.Windows.Forms.NumericUpDown nudBlurGaussian;
        private System.Windows.Forms.RadioButton rbBlurGaussian;
        private System.Windows.Forms.NumericUpDown nudBlur;
        private System.Windows.Forms.RadioButton rbBlur;
        private System.Windows.Forms.Button btKeepChanged;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.NumericUpDown nudErodeX;
        private System.Windows.Forms.NumericUpDown nudErodeY;
        private System.Windows.Forms.RadioButton rbErode;
        private System.Windows.Forms.NumericUpDown nudDilateX;
        private System.Windows.Forms.NumericUpDown nudDilateY;
        private System.Windows.Forms.RadioButton rbDilate;
        private System.Windows.Forms.NumericUpDown nudCloseX;
        private System.Windows.Forms.NumericUpDown nudCloseY;
        private System.Windows.Forms.RadioButton rbClose;
        private System.Windows.Forms.NumericUpDown nudOpenX;
        private System.Windows.Forms.NumericUpDown nudOpenY;
        private System.Windows.Forms.RadioButton rbOpen;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.NumericUpDown nudCThresholdGaussian;
        private System.Windows.Forms.NumericUpDown nudCThresholdMean;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nudTransformY;
        private System.Windows.Forms.NumericUpDown nudTransformX;
        private System.Windows.Forms.CheckBox cTransform;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown nudResizeX;
        private System.Windows.Forms.CheckBox cResize;
        private System.Windows.Forms.NumericUpDown nudResizeY;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton rbFlipY;
        private System.Windows.Forms.RadioButton rbFlipX;
        private System.Windows.Forms.CheckBox cFlip;
        private System.Windows.Forms.NumericUpDown nudSigmaSpaceBilateral;
        private System.Windows.Forms.RadioButton rbBilateralFilter;
        private System.Windows.Forms.NumericUpDown nudDBilateral;
        private System.Windows.Forms.NumericUpDown nudsigmaColorBilateral;
        private System.Windows.Forms.RadioButton rbEqualization;
    }
}