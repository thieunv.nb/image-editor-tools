﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using MvCamCtrl.NET;
using System.Xml;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
namespace Heal
{
    /// <summary>
    /// HiKVision Camera API written by Heal (©2019 FII - IoT Team - 10/09/2019)
    /// </summary>
    public class HikVisionCameraSDK
    {
        private MvCamCtrl.NET.MyCamera ptrCamera;
        private UInt32 m_nBufSizeForDriver = 3072 * 2048 * 3;
        private byte[] m_pBufForDriver = new byte[3072 * 2048 * 3];
        private UInt32 m_nBufSizeForSaveImage = 3072 * 2048 * 3 * 3 + 2048;
        private byte[] m_pBufForSaveImage = new byte[3072 * 2048 * 3 * 3 + 2048];
        private InfoDevice[] mDeviceInfoList;
        private bool isGrabbing = false;
        private bool isRecording = false;
        private bool isOpen = false;
        private MvCamCtrl.NET.MyCamera.MV_CC_RECORD_PARAM mRecoder = new MvCamCtrl.NET.MyCamera.MV_CC_RECORD_PARAM();
        private static HikVisionCameraSDK mCamera;
        private static object lockpad = new object();
        /// <summary>
        /// <para>en: Returns singleton object for SDK</para>
        /// <para>vi: Trả về đối tượng singleton cho SDK</para>
        /// </summary>
        public static HikVisionCameraSDK Instance
        {
            get
            {
                if (mCamera == null)
                {
                    lock (lockpad)
                    {
                        if (mCamera == null)
                        {
                            mCamera = new HikVisionCameraSDK();
                        }
                    }
                }
                return mCamera;
            }
        }
        #region "status camera"

        
        public bool IsOpen
        {
            get { return isOpen; }
        }
        public bool IsRecord
        {
            get { return IsRecord; }
        }
        public bool IsGrab
        {
            get { return isGrabbing; }
        }
        #endregion
        #region "List device and choose device"
        /// <summary>
        /// <para>en: Returns a string array that names all available HIK vision cameras on the current computer if available, null if none </para>
        /// <para>vi: Trả về 1 mảng string tên tất cả các camera HIK vision khả dụng trên máy tính hiện tại nếu có, null nếu không có</para>
        /// </summary>
        /// <returns>null</returns>
        public string[] GetAllCameraName_NET()
        {
            uint nTLayerType = MvCamCtrl.NET.MyCamera.MV_GIGE_DEVICE | MvCamCtrl.NET.MyCamera.MV_USB_DEVICE;
            MvCamCtrl.NET.MyCamera.MV_CC_DEVICE_INFO_LIST stDeviceList = new MvCamCtrl.NET.MyCamera.MV_CC_DEVICE_INFO_LIST();
            int nRet = MvCamCtrl.NET.MyCamera.MV_CC_EnumDevices_NET(nTLayerType, ref stDeviceList);
            if (stDeviceList.nDeviceNum == 0)
                return null;
            else
            {
                mDeviceInfoList = new InfoDevice[stDeviceList.nDeviceNum];
                string[] ListName = new string[stDeviceList.nDeviceNum];
                for (int i=0;i< stDeviceList.nDeviceNum;i++)
                {
                    mDeviceInfoList[i].DeviceInfo = (MvCamCtrl.NET.MyCamera.MV_CC_DEVICE_INFO)Marshal.PtrToStructure(stDeviceList.pDeviceInfo[i], typeof(MvCamCtrl.NET.MyCamera.MV_CC_DEVICE_INFO));
                    if(mDeviceInfoList[i].DeviceInfo.nTLayerType == MvCamCtrl.NET.MyCamera.MV_GIGE_DEVICE)
                    {
                        IntPtr buffer = Marshal.UnsafeAddrOfPinnedArrayElement(mDeviceInfoList[i].DeviceInfo.SpecialInfo.stGigEInfo, 0);
                        MvCamCtrl.NET.MyCamera.MV_GIGE_DEVICE_INFO gigeInfo = (MvCamCtrl.NET.MyCamera.MV_GIGE_DEVICE_INFO)Marshal.PtrToStructure(buffer, typeof(MvCamCtrl.NET.MyCamera.MV_GIGE_DEVICE_INFO));
                        if (gigeInfo.chUserDefinedName != "")
                        {
                            ListName[i] = mDeviceInfoList[i].Name = ("GigE: " + gigeInfo.chUserDefinedName + " (" + gigeInfo.chSerialNumber + ")");
                        }
                        else
                        {
                            ListName[i] = mDeviceInfoList[i].Name = ("GigE: " + gigeInfo.chManufacturerName + " " + gigeInfo.chModelName + " (" + gigeInfo.chSerialNumber + ")");
                        }
                    }
                    else if (mDeviceInfoList[i].DeviceInfo.nTLayerType == MvCamCtrl.NET.MyCamera.MV_USB_DEVICE)
                    {
                        IntPtr buffer = Marshal.UnsafeAddrOfPinnedArrayElement(mDeviceInfoList[i].DeviceInfo.SpecialInfo.stUsb3VInfo, 0);
                        MvCamCtrl.NET.MyCamera.MV_USB3_DEVICE_INFO usbInfo = (MvCamCtrl.NET.MyCamera.MV_USB3_DEVICE_INFO)Marshal.PtrToStructure(buffer, typeof(MvCamCtrl.NET.MyCamera.MV_USB3_DEVICE_INFO));
                        if (usbInfo.chUserDefinedName != "")
                        {
                            ListName[i] = mDeviceInfoList[i].Name = ("USB: " + usbInfo.chUserDefinedName + " (" + usbInfo.chSerialNumber + ")");
                        }
                        else
                        {
                            ListName[i] = mDeviceInfoList[i].Name = ("USB: " + usbInfo.chManufacturerName + " " + usbInfo.chModelName + " (" + usbInfo.chSerialNumber + ")");
                        }
                    }
                }
                return ListName;
            }
        }
        private bool GetMacAddressbyName(string name,ref MvCamCtrl.NET.MyCamera.MV_CC_DEVICE_INFO deviceInfo)
        {
            bool response = false;
            foreach (InfoDevice item in mDeviceInfoList)
            {
                if (name == item.Name)
                {
                    deviceInfo = item.DeviceInfo;
                    response = true;
                }
            }
            return response;
        }
        #endregion
        /// <summary>
        /// <para>en: Display captured picture.If succeeded, return 0; if failed, return Error Code</para>
        /// <para>vn: Hiển thị ảnh đã chụp. Nếu thành công, trả về 0; nếu thất bại, trả về mã lỗi</para>
        /// </summary>
        /// <param name="ptr">Window handle</param>
        /// <returns></returns>
        public int Display_NET(IntPtr ptr)
        {
            if (ptrCamera != null && isGrabbing == true)
            {
                int nRet = ptrCamera.MV_CC_Display_NET(ptr);
                return nRet;
            }
            else
                // camera is not open or grabbing is not start
                return -1;
        }
        /// <summary>
        /// <para>en: Get one picture frame, supports getting chunk information and setting timeout. If succeeded then return Bitmap image; if failed then return null</para>
        /// <para>vi: Nhận một khung hình, hỗ trợ lấy thông tin chunk và đặt thời gian chờ. Nếu thành công thì trả về hình ảnh Bitmap; nếu thất bại thì trả về null</para>
        /// </summary>
        /// <param name="MilsecTimeout">Waiting timeout, int: millisecond</param>
        /// <returns></returns>
        public Bitmap GetOneBitmap_NET(int MilsecTimeout)
        {
            int nRet;
            UInt32 nPayloadSize = 0;
            MyCamera.MVCC_INTVALUE stParam = new MyCamera.MVCC_INTVALUE();
            nRet = ptrCamera.MV_CC_GetIntValue_NET("PayloadSize", ref stParam);
            if (MyCamera.MV_OK != nRet)
            {
                return null;
            }
            nPayloadSize = stParam.nCurValue;
            if (nPayloadSize > m_nBufSizeForDriver)
            {
                m_nBufSizeForDriver = nPayloadSize;
                m_pBufForDriver = new byte[m_nBufSizeForDriver];

                // ch:同时对保存图像的缓存做大小判断处理 | en:Determine the buffer size to save image
                // ch:BMP图片大小：width * height * 3 + 2048(预留BMP头大小) | en:BMP image size: width * height * 3 + 2048 (Reserved for BMP header)
                m_nBufSizeForSaveImage = m_nBufSizeForDriver * 3 + 2048;
                m_pBufForSaveImage = new byte[m_nBufSizeForSaveImage];
            }

            IntPtr pData = Marshal.UnsafeAddrOfPinnedArrayElement(m_pBufForDriver, 0);
            MyCamera.MV_FRAME_OUT_INFO_EX stFrameInfo = new MyCamera.MV_FRAME_OUT_INFO_EX();
            // ch:超时获取一帧，超时时间为1秒 | en:Get one frame timeout, timeout is 1 sec
            nRet = ptrCamera.MV_CC_GetOneFrameTimeout_NET(pData, m_nBufSizeForDriver, ref stFrameInfo, 1000);
            if (MyCamera.MV_OK != nRet)
            {
                return null;
            }

            MyCamera.MvGvspPixelType enDstPixelType;
            if (IsMonoData(stFrameInfo.enPixelType))
            {
                enDstPixelType = MyCamera.MvGvspPixelType.PixelType_Gvsp_Mono8;
            }
            else if (IsColorData(stFrameInfo.enPixelType))
            {
                enDstPixelType = MyCamera.MvGvspPixelType.PixelType_Gvsp_RGB8_Packed;
            }
            else
            {
                return null;
            }

            IntPtr pImage = Marshal.UnsafeAddrOfPinnedArrayElement(m_pBufForSaveImage, 0);
            MyCamera.MV_PIXEL_CONVERT_PARAM stConverPixelParam = new MyCamera.MV_PIXEL_CONVERT_PARAM();
            stConverPixelParam.nWidth = stFrameInfo.nWidth;
            stConverPixelParam.nHeight = stFrameInfo.nHeight;
            stConverPixelParam.pSrcData = pData;
            stConverPixelParam.nSrcDataLen = stFrameInfo.nFrameLen;
            stConverPixelParam.enSrcPixelType = stFrameInfo.enPixelType;
            stConverPixelParam.enDstPixelType = enDstPixelType;
            stConverPixelParam.pDstBuffer = pImage;
            stConverPixelParam.nDstBufferSize = m_nBufSizeForSaveImage;
            nRet = ptrCamera.MV_CC_ConvertPixelType_NET(ref stConverPixelParam);
            if (MyCamera.MV_OK != nRet)
            {
                return null;
            }

            if (enDstPixelType == MyCamera.MvGvspPixelType.PixelType_Gvsp_Mono8)
            {
                //************************Mono8 转 Bitmap*******************************
                Bitmap bmp = new Bitmap(stFrameInfo.nWidth, stFrameInfo.nHeight, stFrameInfo.nWidth * 1, PixelFormat.Format8bppIndexed, pImage);

                ColorPalette cp = bmp.Palette;
                // init palette
                for (int i = 0; i < 256; i++)
                {
                    cp.Entries[i] = Color.FromArgb(i, i, i);
                }
                // set palette back
                bmp.Palette = cp;
                return bmp;
            }
            else
            {
                //*********************RGB8 转 Bitmap**************************
                for (int i = 0; i < stFrameInfo.nHeight; i++)
                {
                    for (int j = 0; j < stFrameInfo.nWidth; j++)
                    {
                        byte chRed = m_pBufForSaveImage[i * stFrameInfo.nWidth * 3 + j * 3];
                        m_pBufForSaveImage[i * stFrameInfo.nWidth * 3 + j * 3] = m_pBufForSaveImage[i * stFrameInfo.nWidth * 3 + j * 3 + 2];
                        m_pBufForSaveImage[i * stFrameInfo.nWidth * 3 + j * 3 + 2] = chRed;
                    }
                }
                try
                {
                    Bitmap bmp = new Bitmap(stFrameInfo.nWidth, stFrameInfo.nHeight, stFrameInfo.nWidth * 3, PixelFormat.Format24bppRgb, pImage);
                    return bmp;
                }
                catch
                {
                }

            }
            return null;
        }
        private void ReceiveImageWorkThread(object obj, uint PayloadSize)
        {
            int nRet = MvCamCtrl.NET.MyCamera.MV_OK;
            MvCamCtrl.NET.MyCamera device = obj as MvCamCtrl.NET.MyCamera;
            MvCamCtrl.NET.MyCamera.MV_FRAME_OUT_INFO_EX stImageInfo = new MvCamCtrl.NET.MyCamera.MV_FRAME_OUT_INFO_EX();
            IntPtr pData = Marshal.AllocHGlobal((int)PayloadSize);
            if (pData == IntPtr.Zero)
            {
                return;
            }
            uint nDataSize = PayloadSize;
            MvCamCtrl.NET.MyCamera.MV_CC_INPUT_FRAME_INFO stInputFrameInfo = new MvCamCtrl.NET.MyCamera.MV_CC_INPUT_FRAME_INFO();
            while (true)
            {
                nRet = device.MV_CC_GetOneFrameTimeout_NET(pData, nDataSize, ref stImageInfo, 1000);
                if (nRet == MvCamCtrl.NET.MyCamera.MV_OK)
                {
                    stInputFrameInfo.pData = pData;
                    stInputFrameInfo.nDataLen = stImageInfo.nFrameLen;
                    nRet = device.MV_CC_InputOneFrame_NET(ref stInputFrameInfo);
                    if (MvCamCtrl.NET.MyCamera.MV_OK != nRet)
                    {
                        Console.WriteLine("Input one frame failed: nRet {0:x8}", nRet);
                    }
                }
                if (!isRecording)
                {
                    Marshal.FreeHGlobal(pData);
                    break;
                }
                
            }
        }
        /// <summary>
        /// <para>en: Start recording. Return 0 on success, and return Error Code on failure.</para>
        /// <para>vi: Bắt đầu thu. Trả về 0 khi thành công và trả về Mã lỗi khi thất bại.</para>
        /// </summary>
        /// <param name="url">
        /// <para>en: Video path with avi format</para>
        /// <para>vi: Đường dẫn video với định dạng avi</para>
        /// </param>
        /// <returns></returns>
        public int StartRecording_NET(string linkVideo)
        {
            FileInfo fileInfo = new FileInfo(linkVideo);
            string url = fileInfo.FullName;
            if(Directory.Exists(fileInfo.DirectoryName))
            {
                Directory.CreateDirectory(fileInfo.DirectoryName);
            }
            int nRet = -1;
            if (ptrCamera != null && isRecording == false)
            {
                //turn off trigger mode
                nRet = ptrCamera.MV_CC_SetEnumValue_NET("TriggerMode", 0);
                if (MvCamCtrl.NET.MyCamera.MV_OK != nRet)
                {
                    return nRet;
                }
                //Get packet size
                MvCamCtrl.NET.MyCamera.MVCC_INTVALUE stParam = new MvCamCtrl.NET.MyCamera.MVCC_INTVALUE();
                nRet = ptrCamera.MV_CC_GetIntValue_NET("PayloadSize", ref stParam);
                uint PayloadSize = stParam.nCurValue;
                if (MvCamCtrl.NET.MyCamera.MV_OK != nRet)
                {
                    return nRet;
                }
                nRet = ptrCamera.MV_CC_GetIntValue_NET("Width", ref stParam);
                if (MvCamCtrl.NET.MyCamera.MV_OK != nRet)
                {
                    return nRet;
                }
                mRecoder.nWidth = (ushort)stParam.nCurValue;
                nRet = ptrCamera.MV_CC_GetIntValue_NET("Height", ref stParam);
                if (MvCamCtrl.NET.MyCamera.MV_OK != nRet)
                {
                    return nRet;
                }
                mRecoder.nHeight = (ushort)stParam.nCurValue;
                MvCamCtrl.NET.MyCamera.MVCC_ENUMVALUE stEnumValue = new MvCamCtrl.NET.MyCamera.MVCC_ENUMVALUE();
                nRet = ptrCamera.MV_CC_GetEnumValue_NET("PixelFormat", ref stEnumValue);
                if (MvCamCtrl.NET.MyCamera.MV_OK != nRet)
                {
                    return nRet;
                }
                mRecoder.enPixelType = (MvCamCtrl.NET.MyCamera.MvGvspPixelType)stEnumValue.nCurValue;
                MvCamCtrl.NET.MyCamera.MVCC_FLOATVALUE stFloatValue = new MvCamCtrl.NET.MyCamera.MVCC_FLOATVALUE();
                nRet = ptrCamera.MV_CC_GetFloatValue_NET("AcquisitionFrameRate", ref stFloatValue);
                if (MvCamCtrl.NET.MyCamera.MV_OK != nRet)
                {
                    return nRet;
                }
                mRecoder.fFrameRate = 30;//stFloatValue.fCurValue > 120?120: stFloatValue.fCurValue;
                mRecoder.nBitRate = 1000;
                mRecoder.enRecordFmtType = MvCamCtrl.NET.MyCamera.MV_RECORD_FORMAT_TYPE.MV_FormatType_AVI;

                mRecoder.strFilePath = url;


                nRet = ptrCamera.MV_CC_StartGrabbing_NET();
                if (MvCamCtrl.NET.MyCamera.MV_OK != nRet)
                {
                    return nRet;
                }

                nRet = ptrCamera.MV_CC_StartRecord_NET(ref mRecoder);
                if(nRet == MvCamCtrl.NET.MyCamera.MV_OK)
                {
                    isRecording = true;
                }
                Thread hReceiveImageThreadHandle = new Thread(() => ReceiveImageWorkThread(ptrCamera, PayloadSize));
                hReceiveImageThreadHandle.Start();
            }
            return nRet;
        }
        /// <summary>
        /// <para>en: Stop recording. Return 0 on success, and return Error Code on failure.</para>
        /// <para>vi: Dừng thu. Trả về 0 khi thành công và trả về Mã lỗi khi thất bại.</para>
        /// </summary>
        /// <returns></returns>
        public int StopRecording_NET()
        {
            int nRet = -1;
            if (ptrCamera != null && isRecording == true)
            {
                nRet = ptrCamera.MV_CC_StopGrabbing_NET();
                if (nRet == MvCamCtrl.NET.MyCamera.MV_OK)
                {
                    isRecording = false;
                }
                nRet = ptrCamera.MV_CC_StopRecord_NET();
            }
            return nRet;
        }
        /// <summary>
        /// <para>en: Start the image acquisition.If succeeded, return 0; if failed, return error Code.</para>
        /// <para>vi: Bắt đầu thu thập hình ảnh. Nếu thành công, trả về 0; nếu thất bại, trả về mã lỗi.</para>
        /// </summary>
        /// <returns></returns>
        public int StartGrabbing_NET()
        {
            int nRet = -1;
            if (isGrabbing == true)
            {
                return 0;
            }
            if (ptrCamera!= null && isGrabbing == false)
            {
                nRet = ptrCamera.MV_CC_StartGrabbing_NET();
                if (nRet == MvCamCtrl.NET.MyCamera.MV_OK)
                    isGrabbing = true;
            }
            return nRet;
        }
        /// <summary>
        /// <para>en: Stop the image acquisition.If succeeded, return 0; if failed, return error Code.</para>
        /// <para>vi: Dừng thu thập hình ảnh. Nếu thành công, trả về 0; nếu thất bại, trả về mã lỗi.</para>
        /// </summary>
        /// <returns></returns>
        public int StopGrabbing_NET()
        {
            int nRet = -1;
            if (ptrCamera != null && isGrabbing == true)
            {
                nRet = ptrCamera.MV_CC_StopGrabbing_NET();
                if (nRet == MvCamCtrl.NET.MyCamera.MV_OK)
                    isGrabbing = false;
            }
            return nRet;
        }
        private  Boolean IsMonoData(MvCamCtrl.NET.MyCamera.MvGvspPixelType enGvspPixelType)
        {
            switch (enGvspPixelType)
            {
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_Mono8:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_Mono10:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_Mono10_Packed:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_Mono12:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_Mono12_Packed:
                    return true;

                default:
                    return false;
            }
        }
        private Boolean IsColorData(MvCamCtrl.NET.MyCamera.MvGvspPixelType enGvspPixelType)
        {
            switch (enGvspPixelType)
            {
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGR8:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerRG8:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGB8:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerBG8:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGR10:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerRG10:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGB10:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerBG10:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGR12:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerRG12:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGB12:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerBG12:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGR10_Packed:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerRG10_Packed:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGB10_Packed:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerBG10_Packed:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGR12_Packed:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerRG12_Packed:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGB12_Packed:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerBG12_Packed:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_RGB8_Packed:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_YUV422_Packed:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_YUV422_YUYV_Packed:
                case MvCamCtrl.NET.MyCamera.MvGvspPixelType.PixelType_Gvsp_YCBCR411_8_CBYYCRYY:
                    return true;

                default:
                    return false;
            }
        }
        /// <summary>
        /// <para>en: Open device (connect to device). Exclusive permission, for other Apps, only reading from CCP register is allowed.</para>
        /// <para>vi: Mở thiết bị (kết nối với thiết bị). Quyền độc quyền, đối với các Ứng dụng khác, chỉ đọc từ đăng ký của ĐCSTQ.</para>
        /// </summary>
        /// <param name="name"></param>
        /// <param name="ImageOnBuffer"></param>
        /// <returns></returns>
        public int OpenCamera_NET(string name, uint ImageOnBuffer = 1)
        {
            if(ptrCamera == null)
                ptrCamera =  new MvCamCtrl.NET.MyCamera();
            if (mDeviceInfoList.Length > 0)
            {
                MvCamCtrl.NET.MyCamera.MV_CC_DEVICE_INFO stDevInfo  = new MvCamCtrl.NET.MyCamera.MV_CC_DEVICE_INFO();
                GetMacAddressbyName(name, ref stDevInfo);
                int nRet = ptrCamera.MV_CC_CreateDevice_NET(ref stDevInfo);
                if (nRet != MvCamCtrl.NET.MyCamera.MV_OK)
                    return nRet;
                bool nAsscessMode = MvCamCtrl.NET.MyCamera.MV_CC_IsDeviceAccessible_NET(ref stDevInfo, MvCamCtrl.NET.MyCamera.MV_ACCESS_Exclusive);
                if (nAsscessMode == false)
                {
                    nRet = ptrCamera.MV_CC_DestroyDevice_NET();
                    return -1;
                }
                else
                {
                    nRet = ptrCamera.MV_CC_OpenDevice_NET(MvCamCtrl.NET.MyCamera.MV_ACCESS_Exclusive, 0);
                    if (nRet != MvCamCtrl.NET.MyCamera.MV_OK)
                    {
                        nRet = ptrCamera.MV_CC_DestroyDevice_NET();
                        return nRet;
                    }
                    else
                    {
                        //Detection network optimal package size(It only works for the GigE camera)
                        int nPacketSize = ptrCamera.MV_CC_GetOptimalPacketSize_NET();
                        ptrCamera.MV_CC_SetImageNodeNum_NET(ImageOnBuffer);
                        ptrCamera.MV_CC_SetIntValue_NET("GevSCPSPacketSize", (uint)nPacketSize);
                        ptrCamera.MV_CC_SetIntValue_NET("AcquisitionMode", 2);
                        ptrCamera.MV_CC_SetIntValue_NET("TriggerMode", 0);
                        isOpen = true;
                        return 0;
                    }
                        
                }
            }
            else
                return -1;
        }
        /// <summary>
        /// <para>en: Close device. If succeeded, return 0; if failed, return Error Code.</para>
        /// <para>vi: Đóng thiết bị. Nếu thành công, trả về 0; nếu thất bại, trả về Mã lỗi.</para>
        /// </summary>
        /// <returns></returns>
        public int CloseCamera_NET()
        {
            int nRet = 0;
            if (ptrCamera != null)
            {
                if(isRecording)
                {
                    nRet = StopRecording_NET();
                    if (MvCamCtrl.NET.MyCamera.MV_OK != nRet)
                    {
                        return nRet;
                    }
                }
                if(isGrabbing)
                {
                    nRet = StopGrabbing_NET();
                    if (MvCamCtrl.NET.MyCamera.MV_OK != nRet)
                    {
                        return nRet;
                    }
                }
                nRet = ptrCamera.MV_CC_CloseDevice_NET();
                if (MvCamCtrl.NET.MyCamera.MV_OK != nRet)
                {
                    return nRet;
                }

                nRet = ptrCamera.MV_CC_DestroyDevice_NET();
                if (MvCamCtrl.NET.MyCamera.MV_OK != nRet)
                {
                    return nRet;
                }
            }
            isOpen = false;
            return nRet;
        }
        public bool FeatureLoad_NET(string pFileName)
        {
            XmlDocument fileDocXml = new XmlDocument();
            try
            {
                fileDocXml.Load(pFileName);
            }
            catch (Exception)
            {
                return false;
            }
            XmlNodeList fileNodeList = fileDocXml.GetElementsByTagName("Feature");
            foreach (XmlNode item in fileNodeList)
            {
                SetDataType type = SetDataType.None;
                switch(item["Type"].InnerText)
                {
                    case "Boolean":
                        { type = SetDataType.Boolean;break; }
                    case "Integer":
                        { type = SetDataType.Integer; break; }
                    case "Enumeration":
                        { type = SetDataType.Enumeration; break; }
                    case "Float":
                        { type = SetDataType.Float; break; }
                }
                if (type == SetDataType.None)
                    continue;
                SetParameter_NET(item["Name"].InnerText,item["Value"].InnerText, type);
            }
            return true;
        }
        /// <summary>
        /// <para>en: Get the value of camera node.</para>
        /// <para>vi: Lấy giá trị của nút camera.</para>
        /// </summary>
        /// <typeparam name="H">type is parameter</typeparam>
        /// <param name="Key"></param>
        /// <param name="value"></param>
        /// <param name="DataType"></param>
        /// <returns></returns>
        public int GetParameter_NET<H>(string Key, ref H value, SetDataType DataType)
        {
            int nRet = MvCamCtrl.NET.MyCamera.MV_OK;
            switch (DataType)
            {
                case SetDataType.Boolean:
                    {
                        bool t = true;
                        nRet = ptrCamera.MV_CC_GetBoolValue_NET(Key,ref t);
                        value = (H)Convert.ChangeType(t, typeof(H));
                        break;
                    }
                case SetDataType.Integer:
                    {
                        MvCamCtrl.NET.MyCamera.MVCC_INTVALUE_EX t = new MvCamCtrl.NET.MyCamera.MVCC_INTVALUE_EX();
                        nRet = ptrCamera.MV_CC_GetIntValueEx_NET(Key, ref t);
                        value = (H)Convert.ChangeType(t.nCurValue, typeof(H));
                        break;
                    }
                case SetDataType.Enumeration:
                    {
                        MvCamCtrl.NET.MyCamera.MVCC_ENUMVALUE t = new MvCamCtrl.NET.MyCamera.MVCC_ENUMVALUE();
                        nRet = ptrCamera.MV_CC_GetEnumValue_NET(Key, ref t);
                        value = (H)Convert.ChangeType(t.nCurValue, typeof(H));
                        break;
                    }
                case SetDataType.Float:
                    {
                        MvCamCtrl.NET.MyCamera.MVCC_FLOATVALUE t = new MvCamCtrl.NET.MyCamera.MVCC_FLOATVALUE();
                        nRet = ptrCamera.MV_CC_GetFloatValue_NET(Key, ref t);
                        value = (H)Convert.ChangeType(t.fCurValue, typeof(H));
                        break;
                    }
            }
            return nRet;
        }
        /// <summary>
        /// <para>en: Set the value for camera node.</para>
        /// <para>vi: Đặt giá trị cho nút camera.</para>
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="value"></param>
        /// <param name="DataType"></param>
        /// <returns></returns>
        public int SetParameter_NET(string Key, object value, SetDataType DataType)
        {
            int nRet = MvCamCtrl.NET.MyCamera.MV_OK;
            switch (DataType)
            {
                case SetDataType.Boolean:
                    {
                        bool v = (Convert.ToInt16(value) == 1) ? true : false;
                        nRet = ptrCamera.MV_CC_SetBoolValue_NET(Key, v);
                        break;
                    }
                case SetDataType.Integer:
                    {
                        nRet = ptrCamera.MV_CC_SetIntValueEx_NET(Key, Convert.ToInt64(value));
                        break;
                    }
                case SetDataType.Enumeration:
                    {
                        nRet = ptrCamera.MV_CC_SetEnumValue_NET(Key, Convert.ToUInt32(value));
                        break;
                    }
                case SetDataType.Float:
                    {
                        float v = (float)Convert.ToDecimal(value);
                        nRet = ptrCamera.MV_CC_SetFloatValue_NET(Key, v);
                        break;
                    }
                case SetDataType.Command:
                    {
                        nRet = ptrCamera.MV_CC_SetCommandValue_NET(Key);
                        break;
                    }
            }
            return nRet;
        }
    }
    struct InfoDevice
    {
        public string Name { get; set; }
        public MvCamCtrl.NET.MyCamera.MV_CC_DEVICE_INFO DeviceInfo { get; set; }

    }
    public enum SetDataType
    {
        None,
        Boolean,
        Integer,
        Enumeration,
        Float,
        Command
    }
    enum TriggerSource
    {
        Line0 = 0,
        Line1 = 1,
        Line2 = 2,
        Line3 = 3,
        Counter0 = 4,
        Software = 7,
        FrequencyConverter = 8,
    }
}
