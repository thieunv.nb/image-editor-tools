﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using System.Drawing;
using Emgu.CV.Structure;


namespace Heal
{
    class UtilsConfigurations
    {
        public static Image<Bgr, byte> ImageSource;
        public static Image<Gray, byte> ImageGray;
        public static Image<Bgr, byte> ImgEqualization;
        public static Image<Bgr, byte> ImageBlur;
        public static Image<Bgr, byte> ImageBlurGaussian;
        public static Image<Bgr, byte> ImageBlurMedian;
        public static Image<Bgr, byte> ImgBilateral;
        public static Image<Gray, byte> ImageThreshold;
        public static Image<Gray, byte> ImageThresholdInv;
        public static Image<Gray, byte> ImageThresholdGaussian;
        public static Image<Gray, byte> ImageThresholdMean;
        public static Image<Gray, byte> ImageThresholdOstu;
        public static Image<Gray, byte> ImageLaplacian;
        public static Image<Gray, byte> ImageSobelX;
        public static Image<Gray, byte> ImageSobelY;
        public static Image<Gray, byte> ImageCanny;
        public static Image<Gray, byte> ImageDilate;
        public static Image<Gray, byte> ImageErode;
        public static Image<Gray, byte> ImageOpen;
        public static Image<Gray, byte> ImageClose;
        public static Size DilateKernel = new Size(5, 5);
        public static Size ErodeKernel = new Size(5, 5);
        public static Size OpenKernel = new Size(5, 5);
        public static Size CloseKernel = new Size(5, 5);
        public static int Blur = 5;
        public static int BlurGaussian = 5;
        public static int BlurMedian = 5;
        public static int DBilateral = 9;
        public static int SigmaColorBilateral = 11;
        public static int SigmaSpaceBilateral = 11;
        public static int Threshold = 127;
        public static int ThresholdGaussian = 127;
        public static int CThresholdGaussian = 0;
        public static int ThresholdMean = 127;
        public static int CThresholdMean = 0;
        public static int KLaplacian = 1;
        public static int ThresholdInv = 127;
        public static int KSobelX = 1;
        public static int KSobelY = 1;
        public static int MaxCanny = 100;
        public static int MinCanny = 200;

        public static void SetImage(Image<Bgr, byte> ImgSource)
        {
            if (ImageSource != null)
                ImageSource.Dispose();
            ImageSource = ImgSource.Copy();
        }
        public static void Processing(modeProcessing mode)
        {
            try
            {
                if (ImageGray != null)
                {
                    ImageGray.Dispose();
                    ImageGray = null;
                }
                    
                if (ImageSource != null && ImageSource.Ptr != IntPtr.Zero)
                {
                    //lock(ImageGray)
                    {
                        ImageGray = new Image<Gray, byte>(ImageSource.Size);
                        CvInvoke.CvtColor(ImageSource, ImageGray, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
                    }
                    switch (mode)
                    {
                        case modeProcessing.ImgEqualization:
                            if (ImgEqualization != null)
                            {
                                ImgEqualization.Dispose();
                                ImgEqualization = null;
                            }
                            {
                                ImgEqualization = ImageSource.Copy();
                                
                                using (Image<Hsv, byte> tempHsv = ImgEqualization.Convert<Hsv, byte>())
                                {
                                    using (Image<Gray, byte> imageV = tempHsv[2])
                                    {
                                        imageV._EqualizeHist();
                                        tempHsv[2] = imageV;
                                        CvInvoke.CvtColor(tempHsv, ImgEqualization, Emgu.CV.CvEnum.ColorConversion.Hsv2Bgr);
                                    }
                                }
                            }
                            break;
                        case modeProcessing.ImageBlur:
                            if (ImageBlur != null)
                            {
                                ImageBlur.Dispose();
                                ImageBlur = null;
                            }
                            {
                                ImageBlur = new Image<Bgr, byte>(ImageSource.Size);
                                CvInvoke.Blur(ImageSource, ImageBlur, new System.Drawing.Size(Blur, Blur), new System.Drawing.Point(-1, -1));
                            }
                            break;
                        case modeProcessing.ImageBlurGaussian:
                            if (ImageBlurGaussian != null)
                            {
                                ImageBlurGaussian.Dispose();
                                ImageBlurGaussian = null;
                            }
                            {
                                ImageBlurGaussian = new Image<Bgr, byte>(ImageSource.Size);
                                CvInvoke.GaussianBlur(ImageSource, ImageBlurGaussian, new System.Drawing.Size(BlurGaussian, BlurGaussian), 0);
                            }
                            break;
                        case modeProcessing.ImageBlurMedian:
                            if (ImageBlurMedian != null)
                            {
                                ImageBlurMedian.Dispose();
                                ImageBlurMedian = null;
                            }
                            {
                                ImageBlurMedian = new Image<Bgr, byte>(ImageSource.Size);
                                CvInvoke.MedianBlur(ImageSource, ImageBlurMedian, BlurMedian);
                            }
                            break;
                        case modeProcessing.ImgBilateral:
                            if (ImgBilateral != null)
                            {
                                ImgBilateral.Dispose();
                                ImgBilateral = null;
                            }
                            {
                                ImgBilateral = new Image<Bgr, byte>(ImageSource.Size);
                                CvInvoke.BilateralFilter(ImageSource, ImgBilateral, DBilateral, SigmaColorBilateral, SigmaSpaceBilateral);
                            }
                            break;
                        case modeProcessing.ImageThreshold:
                            if (ImageThreshold != null)
                            {
                                ImageThreshold.Dispose();
                                ImageThreshold = null;
                            }
                            {
                                ImageThreshold = new Image<Gray, byte>(ImageSource.Size);
                                CvInvoke.Threshold(ImageGray, ImageThreshold, Threshold, 255, Emgu.CV.CvEnum.ThresholdType.Binary);
                            }
                            
                            break;
                        case modeProcessing.ImageThresholdInv:
                            if (ImageThresholdInv != null)
                            {
                                ImageThresholdInv.Dispose();
                                ImageThresholdInv = null;
                            }
                            //lock(ImageThresholdInv)
                            {
                                ImageThresholdInv = new Image<Gray, byte>(ImageSource.Size);
                                CvInvoke.Threshold(ImageGray, ImageThresholdInv, ThresholdInv, 255, Emgu.CV.CvEnum.ThresholdType.BinaryInv);
                            }
                            break;
                        case modeProcessing.ImageThresholdGaussian:
                            if (ImageThresholdGaussian != null)
                            {
                                ImageThresholdGaussian.Dispose();
                                ImageThresholdGaussian = null;
                            }
                            //lock(ImageThresholdGaussian)
                            {
                                ImageThresholdGaussian = new Image<Gray, byte>(ImageSource.Size);
                                CvInvoke.AdaptiveThreshold(ImageGray, ImageThresholdGaussian, 255, Emgu.CV.CvEnum.AdaptiveThresholdType.GaussianC, Emgu.CV.CvEnum.ThresholdType.Binary, ThresholdGaussian, CThresholdGaussian);
                            }
                            break;
                        case modeProcessing.ImageThresholdMean:
                            if (ImageThresholdMean != null)
                            {
                                ImageThresholdMean.Dispose();
                                ImageThresholdMean = null;
                            }
                            //lock(ImageThresholdGaussian)
                            {
                                ImageThresholdMean = new Image<Gray, byte>(ImageSource.Size);
                                CvInvoke.AdaptiveThreshold(ImageGray, ImageThresholdMean, 255, Emgu.CV.CvEnum.AdaptiveThresholdType.MeanC, Emgu.CV.CvEnum.ThresholdType.Binary, ThresholdMean, CThresholdMean);
                            }
                            break;
                        case modeProcessing.ImageThresholdOstu:
                            if (ImageThresholdOstu != null)
                            {
                                ImageThresholdOstu.Dispose();
                                ImageThresholdOstu = null;
                            }
                            //lock(ImageThresholdOstu)
                            {
                                ImageThresholdOstu = new Image<Gray, byte>(ImageSource.Size);
                                CvInvoke.Threshold(ImageGray, ImageThresholdOstu, Threshold, 255, Emgu.CV.CvEnum.ThresholdType.Otsu);
                            }
                            break;
                        case modeProcessing.ImageLaplacian:
                            if (ImageLaplacian != null)
                            {
                                ImageLaplacian.Dispose();
                                ImageLaplacian = null;
                            }
                            //lock(ImageLaplacian)
                            {
                                ImageLaplacian = new Image<Gray, byte>(ImageSource.Size);
                                CvInvoke.Laplacian(ImageGray, ImageLaplacian, Emgu.CV.CvEnum.DepthType.Cv8U, KLaplacian);
                            }
                            break;
                        case modeProcessing.ImageSobelX:
                            if (ImageSobelX != null)
                            {
                                ImageSobelX.Dispose();
                                ImageSobelX = null;
                            }
                            ImageSobelX = new Image<Gray, byte>(ImageSource.Size);
                            CvInvoke.Sobel(ImageGray, ImageSobelX, Emgu.CV.CvEnum.DepthType.Cv8U, 1, 0, KSobelX);
                            break;
                        case modeProcessing.ImageSobelY:
                            if (ImageSobelY != null)
                            {
                                ImageSobelY.Dispose();
                                ImageSobelY = null;
                            }
                            //lock(ImageSobelY)
                            {
                                ImageSobelY = new Image<Gray, byte>(ImageSource.Size);
                                CvInvoke.Sobel(ImageGray, ImageSobelY, Emgu.CV.CvEnum.DepthType.Cv8U, 0, 1, KSobelY);
                            }
                            
                            break;
                        case modeProcessing.ImageCanny:
                            if (ImageCanny != null)
                            {
                                ImageCanny.Dispose();
                                ImageCanny = null;
                            }
                               //lock(ImageCanny)
                            {
                                ImageCanny = new Image<Gray, byte>(ImageSource.Size);
                                CvInvoke.Canny(ImageGray, ImageCanny, MinCanny, MaxCanny);
                            }
                            
                            break;
                        case modeProcessing.ImageDilate:
                            if (ImageDilate != null)
                            {
                                ImageDilate.Dispose();
                                ImageDilate = null;
                            }
                            ImageDilate = new Image<Gray, byte>(ImageSource.Size);
                            using (Mat k = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, DilateKernel, new Point(-1, -1)))
                            {
                                CvInvoke.Dilate(ImageGray, ImageDilate, k, new Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar());
                            }
                            break;
                        case modeProcessing.ImageErode:
                            if (ImageErode != null)
                            {
                                ImageErode.Dispose();
                                ImageErode = null;
                            }
                            ImageErode = new Image<Gray, byte>(ImageSource.Size);
                            using (Mat k = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, ErodeKernel, new Point(-1, -1)))
                            {
                                CvInvoke.Erode(ImageGray, ImageErode, k, new Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar());
                            }
                            break;
                        case modeProcessing.ImageOpen:
                            if (ImageOpen != null)
                            {
                                ImageOpen.Dispose();
                                ImageOpen = null;
                            }
                            ImageOpen = new Image<Gray, byte>(ImageSource.Size);
                            using (Mat k = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, OpenKernel, new Point(-1, -1)))
                            {
                                CvInvoke.MorphologyEx(ImageGray, ImageOpen, Emgu.CV.CvEnum.MorphOp.Open, k, new Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar());
                            }
                            break;
                        case modeProcessing.ImageClose:
                            if (ImageClose != null)
                            {
                                ImageClose.Dispose();
                                ImageClose = null;
                            }
                            ImageClose = new Image<Gray, byte>(ImageSource.Size);
                            using (Mat k = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, CloseKernel, new Point(-1, -1)))
                            {
                                CvInvoke.MorphologyEx(ImageGray, ImageClose, Emgu.CV.CvEnum.MorphOp.Close, k, new Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar());
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception)
            {

            }
        }
        public static int RotationImage(ref Image<Bgr, byte> scr, System.Drawing.Point Center, double Angle)
        {
            Angle = Angle * 180.0 / Math.PI;
            Mat rotMatrix = new Mat();
            CvInvoke.GetRotationMatrix2D(Center, -Angle, 1, rotMatrix);
            CvInvoke.WarpAffine(scr, scr, rotMatrix, scr.Size);
            rotMatrix.Dispose();
            rotMatrix = null;
            return 0;
        }
        public static int RotationImage(ref Image<Gray, byte> scr, System.Drawing.Point Center, double Angle)
        {
            Angle = Angle * 180.0 / Math.PI;
            Mat rotMatrix = new Mat();
            CvInvoke.GetRotationMatrix2D(Center, -Angle, 1, rotMatrix);
            CvInvoke.WarpAffine(scr, scr, rotMatrix, scr.Size);
            rotMatrix.Dispose();
            rotMatrix = null;
            return 0;
        }
        public static int ImageTransformation(ref Image<Bgr, byte> scr, int X, int Y)
        {
            float[,] translationArray = { { 1, 0, X }, { 0, 1, Y } };
            Matrix<float> translationMatrix = new Matrix<float>(translationArray);
            CvInvoke.WarpAffine(scr, scr, translationMatrix, scr.Size);
            translationMatrix.Dispose();
            translationMatrix = null;
            return 0;
        }
        public static int ImageTransformation(ref Image<Gray, byte> scr, int X, int Y)
        {
            float[,] translationArray = { { 1, 0, X }, { 0, 1, Y } };
            Matrix<float> translationMatrix = new Matrix<float>(translationArray);
            CvInvoke.WarpAffine(scr, scr, translationMatrix, scr.Size);
            translationMatrix.Dispose();
            translationMatrix = null;
            return 0;
        }
    }
}
