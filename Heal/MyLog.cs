﻿using NLog;
using NLog.Targets;
using NLog.Config;

namespace Heal
{
    /// <summary>
    /// Configuration Nlog by Heal 12/19/2019.
    /// <para>Save log by day.</para>
    /// <para> Show all level to console window</para>
    /// <para>Write Error, Warn, Debug, Fatal, Off, and Trace level to log file.</para>
    /// </summary>
    public class MyLog
    {
        private static Logger Logger;
        private static MyLog mlog;
        /// <summary>
        /// <para>en: Initialization configuration for writing to file </para>
        /// <para>vi: Cấu hình khởi tạo để ghi vào tập tin</para>
        /// </summary>
        public static void Initialization(bool Info = false, bool Debug = true, bool Warn = true, bool Error = true, bool Trace = true, bool Off = true, bool Fatal = true)
        {
            LoggingConfiguration config = new LoggingConfiguration();
            //config for console log
            ColoredConsoleTarget consoleTarget = new ColoredConsoleTarget("ConsoleTarget")
            {
                Layout = @"${longdate}|${level}|${message} ${exception}"
            };
            config.AddTarget(consoleTarget);
            //config for file log
            FileTarget fileTarget = new FileTarget("FileTarget")
            {
                FileName = "${basedir}/log/${date:format=yyyy_MM_dd}.log",
                Layout = "${longdate}|${level}|${message}${callsite:className=false:fileName=true:includeSourcePath=false:methodName=false}|${exception}"
            };
            config.AddTarget(fileTarget);
            // errors and warning write to file
            if(Info)
                config.AddRuleForOneLevel(LogLevel.Info, fileTarget);
            if(Error)
            config.AddRuleForOneLevel(LogLevel.Error, fileTarget); 
            if(Warn)
            config.AddRuleForOneLevel(LogLevel.Warn, fileTarget);
            if(Debug)
            config.AddRuleForOneLevel(LogLevel.Debug, fileTarget);
            if(Fatal)
            config.AddRuleForOneLevel(LogLevel.Fatal, fileTarget);
            if(Off)
            config.AddRuleForOneLevel(LogLevel.Off, fileTarget);
            if(Trace)
            config.AddRuleForOneLevel(LogLevel.Trace, fileTarget);
            // all to console
            config.AddRuleForAllLevels(consoleTarget); 
            //Activate the configuration
            LogManager.Configuration = config;
        }
        /// <summary>
        /// <para>en: Return single tone NLog object</para>
        /// <para>vi: Trả về đối tượng NLog giai điệu đơn</para>
        /// </summary>
        /// <example>
        /// <code>
        /// Logger log = MyLog.Instance;
        /// log.Error("Line Error");
        /// log.Info("Line Info");
        /// </code>
        /// </example>
        public static MyLog Instance
        {
            get
            {
                if(mlog == null)
                {
                    mlog = new MyLog();
                    if (Logger == null)
                    {
                        Logger = LogManager.GetLogger("Debug");
                        Initialization();
                    }
                }
                return mlog;
            }
        }
        public void Info(string msg)
        {
            Logger.Info(msg);
        }
        public void Error(string msg)
        {
            Logger.Error(msg);
        }
        public void Debug(string msg)
        {
            Logger.Debug(msg);
        }
        public void Warn(string msg)
        {
            Logger.Warn(msg);
        }
        public void Trace(string msg)
        {
            Logger.Trace(msg);
        }
        public void Fatal(string msg)
        {
            Logger.Fatal(msg);
        }
    }
}
