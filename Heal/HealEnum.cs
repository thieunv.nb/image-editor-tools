﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heal
{
    enum modeProcessing
    {
        ImageSource
        , ImageGray
        , ImgEqualization
        , ImageBlur
        , ImageBlurGaussian
        , ImageBlurMedian
        , ImgBilateral
        , ImageThreshold
        , ImageThresholdInv
        , ImageThresholdGaussian
        , ImageThresholdMean
        , ImageThresholdOstu
        , ImageLaplacian
        , ImageSobelX
        , ImageSobelY
        , ImageCanny
        , ImageDilate
        , ImageErode
        , ImageOpen
        , ImageClose

    }
}
