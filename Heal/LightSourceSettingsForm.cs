﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace Heal
{
    public partial class LightSourceSettingsForm : Form
    {
        private DKZController mDKZ = DKZController.Instance;
        public LightSourceSettingsForm()
        {
            InitializeComponent();
        }

        private void LightSourceSettingsForm_Load(object sender, EventArgs e)
        {
            string[] _port = SerialPort.GetPortNames();
            if(_port != null)
            {
                cbPort.Items.AddRange(_port);
                if(cbPort.Items.Count > 0)
                    cbPort.SelectedIndex = 0;
            }
            // update  ui
            updateUI(false);
        }
        private void updateUI(bool mode)
        {
            btOpen.Enabled = !mode;
            nBaud.Enabled = !mode;
            cbPort.Enabled = !mode;
            btClose.Enabled = mode;
            nChannel1.Enabled = mode;
            nChannel2.Enabled = mode;
            nChannel3.Enabled = mode;
            nChannel4.Enabled = mode;
        }
        private void nBaud_ValueChanged(object sender, EventArgs e)
        {
            int baud = (int)nBaud.Value;
            mDKZ.Serial.BaudRate = baud;
        }

        private void cbPort_SelectedIndexChanged(object sender, EventArgs e)
        {
            mDKZ.Serial.PortName = cbPort.SelectedItem.ToString();
        }

        private void btOpen_Click(object sender, EventArgs e)
        {
            int res = mDKZ.Open();
            if(res == 0)

            {
                updateUI(true);
            }
            else
            {
                MessageBox.Show("Tao đell bật nổi " + cbPort.SelectedItem.ToString() +", mã lỗi là 0x" + res.ToString("X8"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            int res = mDKZ.Close();
            if (res == 0)

            {
                updateUI(false);
            }
            else
            {
                MessageBox.Show("Tao đell tắt nổi " + cbPort.SelectedItem.ToString() + ", mã lỗi là 0x" + res.ToString("X8"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void nChannel1_ValueChanged(object sender, EventArgs e)
        {
            int value = (int)nChannel1.Value;
            mDKZ.SetOne(1, value);
        }

        private void nChannel2_ValueChanged(object sender, EventArgs e)
        {
            int value = (int)nChannel2.Value;
            mDKZ.SetOne(2, value);
        }

        private void nChannel3_ValueChanged(object sender, EventArgs e)
        {
            int value = (int)nChannel3.Value;
            mDKZ.SetOne(3, value);
        }

        private void nChannel4_ValueChanged(object sender, EventArgs e)
        {
            int value = (int)nChannel4.Value;
            mDKZ.SetOne(4, value);
        }

        private void LightSourceSettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
           if(mDKZ.Serial.IsOpen)
                mDKZ.Close();
        }
    }
}
