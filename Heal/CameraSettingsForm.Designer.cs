﻿namespace Heal
{
    partial class CameraSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CameraSettingsForm));
            this.imageBox = new Emgu.CV.UI.ImageBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panelSettings = new System.Windows.Forms.Panel();
            this.nExposureTime = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btBrowser = new System.Windows.Forms.Button();
            this.txtPathSettingsFile = new System.Windows.Forms.TextBox();
            this.btReloadCamera = new System.Windows.Forms.Button();
            this.btCloseCamera = new System.Windows.Forms.Button();
            this.btOpenCamera = new System.Windows.Forms.Button();
            this.cbListCamera = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panelSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nExposureTime)).BeginInit();
            this.SuspendLayout();
            // 
            // imageBox
            // 
            this.imageBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.imageBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox.Location = new System.Drawing.Point(12, 12);
            this.imageBox.Name = "imageBox";
            this.imageBox.Size = new System.Drawing.Size(637, 537);
            this.imageBox.TabIndex = 2;
            this.imageBox.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panelSettings);
            this.groupBox1.Controls.Add(this.btReloadCamera);
            this.groupBox1.Controls.Add(this.btCloseCamera);
            this.groupBox1.Controls.Add(this.btOpenCamera);
            this.groupBox1.Controls.Add(this.cbListCamera);
            this.groupBox1.Location = new System.Drawing.Point(655, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(217, 537);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Options";
            // 
            // panelSettings
            // 
            this.panelSettings.Controls.Add(this.nExposureTime);
            this.panelSettings.Controls.Add(this.label2);
            this.panelSettings.Controls.Add(this.label1);
            this.panelSettings.Controls.Add(this.btBrowser);
            this.panelSettings.Controls.Add(this.txtPathSettingsFile);
            this.panelSettings.Location = new System.Drawing.Point(0, 75);
            this.panelSettings.Name = "panelSettings";
            this.panelSettings.Size = new System.Drawing.Size(217, 462);
            this.panelSettings.TabIndex = 4;
            // 
            // nExposureTime
            // 
            this.nExposureTime.Location = new System.Drawing.Point(93, 70);
            this.nExposureTime.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.nExposureTime.Minimum = new decimal(new int[] {
            34,
            0,
            0,
            0});
            this.nExposureTime.Name = "nExposureTime";
            this.nExposureTime.Size = new System.Drawing.Size(118, 20);
            this.nExposureTime.TabIndex = 8;
            this.nExposureTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nExposureTime.Value = new decimal(new int[] {
            34,
            0,
            0,
            0});
            this.nExposureTime.ValueChanged += new System.EventHandler(this.nExposureTime_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Exposure Time:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "File Settings (.mfs)";
            // 
            // btBrowser
            // 
            this.btBrowser.Location = new System.Drawing.Point(180, 26);
            this.btBrowser.Name = "btBrowser";
            this.btBrowser.Size = new System.Drawing.Size(31, 23);
            this.btBrowser.TabIndex = 5;
            this.btBrowser.Text = "...";
            this.btBrowser.UseVisualStyleBackColor = true;
            this.btBrowser.Click += new System.EventHandler(this.btBrowser_Click);
            // 
            // txtPathSettingsFile
            // 
            this.txtPathSettingsFile.Location = new System.Drawing.Point(6, 29);
            this.txtPathSettingsFile.Name = "txtPathSettingsFile";
            this.txtPathSettingsFile.ReadOnly = true;
            this.txtPathSettingsFile.Size = new System.Drawing.Size(168, 20);
            this.txtPathSettingsFile.TabIndex = 0;
            // 
            // btReloadCamera
            // 
            this.btReloadCamera.Image = ((System.Drawing.Image)(resources.GetObject("btReloadCamera.Image")));
            this.btReloadCamera.Location = new System.Drawing.Point(180, 46);
            this.btReloadCamera.Name = "btReloadCamera";
            this.btReloadCamera.Size = new System.Drawing.Size(31, 23);
            this.btReloadCamera.TabIndex = 3;
            this.btReloadCamera.UseVisualStyleBackColor = true;
            this.btReloadCamera.Click += new System.EventHandler(this.btReloadCamera_Click);
            // 
            // btCloseCamera
            // 
            this.btCloseCamera.Location = new System.Drawing.Point(93, 46);
            this.btCloseCamera.Name = "btCloseCamera";
            this.btCloseCamera.Size = new System.Drawing.Size(81, 23);
            this.btCloseCamera.TabIndex = 2;
            this.btCloseCamera.Text = "Close Camera";
            this.btCloseCamera.UseVisualStyleBackColor = true;
            this.btCloseCamera.Click += new System.EventHandler(this.btCloseCamera_Click);
            // 
            // btOpenCamera
            // 
            this.btOpenCamera.Location = new System.Drawing.Point(6, 46);
            this.btOpenCamera.Name = "btOpenCamera";
            this.btOpenCamera.Size = new System.Drawing.Size(81, 23);
            this.btOpenCamera.TabIndex = 1;
            this.btOpenCamera.Text = "Open Camera";
            this.btOpenCamera.UseVisualStyleBackColor = true;
            this.btOpenCamera.Click += new System.EventHandler(this.btOpenCamera_Click);
            // 
            // cbListCamera
            // 
            this.cbListCamera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbListCamera.FormattingEnabled = true;
            this.cbListCamera.Location = new System.Drawing.Point(6, 19);
            this.cbListCamera.Name = "cbListCamera";
            this.cbListCamera.Size = new System.Drawing.Size(205, 21);
            this.cbListCamera.TabIndex = 0;
            // 
            // CameraSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.imageBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(900, 600);
            this.Name = "CameraSettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Camera Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CameraSettingsForm_FormClosing);
            this.Load += new System.EventHandler(this.CameraSettingsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imageBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.panelSettings.ResumeLayout(false);
            this.panelSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nExposureTime)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Emgu.CV.UI.ImageBox imageBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btReloadCamera;
        private System.Windows.Forms.Button btCloseCamera;
        private System.Windows.Forms.Button btOpenCamera;
        private System.Windows.Forms.ComboBox cbListCamera;
        private System.Windows.Forms.Panel panelSettings;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btBrowser;
        private System.Windows.Forms.TextBox txtPathSettingsFile;
        private System.Windows.Forms.NumericUpDown nExposureTime;
        private System.Windows.Forms.Label label2;
    }
}